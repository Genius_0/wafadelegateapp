package com.batteryhome.orderslibrary.Customer_Orders_Package;

/**
 * Created by ali on 01-Apr-18.
 */

public class OrderDetailsInfo {
    private int orderProductID;
    private int productID;
    private String productDescription;
    private float discount;
    private float price;
    private int quantity;

    public OrderDetailsInfo(int orderProductID, int productID, String productDescription, float price,float discount, int quantity) {
        this.orderProductID = orderProductID;
        this.productID = productID;
        this.productDescription = productDescription;
        this.price = price;
        this.discount = discount;
        this.quantity = quantity;
    }

    public int getOrderProductID() {
        return orderProductID;
    }

    public void setOrderProductID(int orderProductID) {
        this.orderProductID = orderProductID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
