package com.batteryhome.orderslibrary.Orders_Package;

/**
 * Created by ali on 05-Apr-18.
 */

public class OrderInfo {
    private int orderID;
    private int addressID;
    private int customerID;
    private String customerName;
    private String date;

    public OrderInfo(int orderID, int addressID, int customerID, String customerName, String date) {
        this.orderID = orderID;
        this.addressID = addressID;
        this.customerID = customerID;
        this.customerName = customerName;
        this.date = date;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

