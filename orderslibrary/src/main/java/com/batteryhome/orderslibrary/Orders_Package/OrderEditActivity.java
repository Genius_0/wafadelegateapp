package com.batteryhome.orderslibrary.Orders_Package;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.batteryhome.orderslibrary.Callback_Package.OrderBatteriesCallback;
import com.batteryhome.orderslibrary.Callback_Package.OrderTiresCallback;
import com.batteryhome.orderslibrary.Callback_Package.RemoveOrderCallback;
import com.batteryhome.orderslibrary.Callback_Package.RemoveProductFromOrderCallback;
import com.batteryhome.orderslibrary.Callback_Package.SubmitOrderCallback;
import com.batteryhome.orderslibrary.Customer_Orders_Package.OrderDetailsAdapter;
import com.batteryhome.orderslibrary.Customer_Orders_Package.OrderDetailsInfo;
import com.batteryhome.orderslibrary.Loading;
import com.batteryhome.orderslibrary.MyID;
import com.batteryhome.orderslibrary.ProductsActivityInOrder;
import com.batteryhome.orderslibrary.R;
import com.batteryhome.orderslibrary.ServerData_Package.GetOrderBatteriesByOrderID;
import com.batteryhome.orderslibrary.ServerData_Package.GetOrderTiresByOrderID;
import com.batteryhome.orderslibrary.ServerData_Package.RemoveOrder;
import com.batteryhome.orderslibrary.ServerData_Package.RemoveProductFromOrder;
import com.batteryhome.orderslibrary.ServerData_Package.SubmitOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrderEditActivity extends AppCompatActivity {
    private ListView listView;
    private List<OrderDetailsInfo> orderDetailsList =new ArrayList<>();
    private OrderDetailsAdapter adapter;
    private String orderID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_edit);
        listView = (ListView) findViewById(R.id.order_edit_listVew);
        adapter = new OrderDetailsAdapter(getApplicationContext(),orderDetailsList);
        listView.setAdapter(adapter);
        final Loading loading = new Loading(OrderEditActivity.this);
        try {
            orderID = getIntent().getStringExtra("orderID");
        }catch (Exception e){
            e.printStackTrace();
        }
        setTitle("رقم الطلبية : "+orderID);
        loading.show();
        GetOrderBatteriesByOrderID orderBatteries = new GetOrderBatteriesByOrderID(orderID, new OrderBatteriesCallback() {
            @Override
            public void onDownloadFinished(String result) {
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            orderDetailsList.add(new OrderDetailsInfo(
                                    jo.getInt("product_orderID"),
                                    jo.getInt("ProductID"),
                                    jo.getString("brandName")+"\n"+jo.getString("sizeName")+"\n"+jo.getString("modelName")+"\n"+jo.getString("type")+" - "+jo.getString("ampere")+"A",
                                    (float) jo.getDouble("price"), (float) jo.getDouble("discount"),jo.getInt("quantity")
                            ));
                            adapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"لا يوجد بطاريات في الطلبية",Toast.LENGTH_SHORT).show();
                }
                GetOrderTiresByOrderID orderTires = new GetOrderTiresByOrderID(orderID, new OrderTiresCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        loading.hide();
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    orderDetailsList.add(new OrderDetailsInfo(
                                            jo.getInt("product_orderID"),
                                            jo.getInt("ProductID"),
                                            jo.getString("brandName")+"\n"+jo.getString("width")+"/"+jo.getString("ratio")+"R"+jo.getString("diameter")+"\n"+jo.getString("countryName"),
                                            (float) jo.getDouble("price"), (float) jo.getDouble("discount"),jo.getInt("quantity")
                                    ));
                                    adapter.notifyDataSetChanged();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لا يوجد إطارات في الطلبية",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                orderTires.execute();
            }
        });
        orderBatteries.execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                final OrderDetailsInfo info = (OrderDetailsInfo) adapterView.getItemAtPosition(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(OrderEditActivity.this);
                builder.setTitle("هل تريد حذف هذا المنتج من الطلبية؟");
                builder.setNegativeButton("الغاء",null);
                builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Loading removeLoading = new Loading(OrderEditActivity.this,"جاري الحذف");
                        removeLoading.show();
                        RemoveProductFromOrder removeProductFromOrder = new RemoveProductFromOrder(new MyID(getApplicationContext()).getID(), info.getOrderProductID() + "", new RemoveProductFromOrderCallback() {
                            @Override
                            public void onDownloadFinished(String result) {
                                removeLoading.hide();
                                if(result.equals("SUCCESS")){
                                    Toast.makeText(getApplicationContext(),"تم الحذف",Toast.LENGTH_SHORT).show();
                                    orderDetailsList.remove(position);
                                    adapter.notifyDataSetChanged();
                                }else {
                                    Toast.makeText(getApplicationContext(),"لم يتم الحذف",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        removeProductFromOrder.execute();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.order_edit_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.order_edit_menu_add_product){
            Intent intent= new Intent(getApplicationContext(), ProductsActivityInOrder.class);
            intent.putExtra("orderID",""+orderID);
            startActivityForResult(intent,777);
        }else if (id==R.id.order_edit_menu_submit_order){
            if(orderDetailsList.size()>0){
                AlertDialog.Builder builder = new AlertDialog.Builder(OrderEditActivity.this);
                builder.setTitle("تنفيذ الطلبية ؟");
                builder.setPositiveButton("تنفيذ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Loading loading = new Loading(OrderEditActivity.this,"جاري تنفيذ الطلبية");
                        loading.show();
                        SubmitOrder submitOrder = new SubmitOrder(new MyID(getApplicationContext()).getID(), orderID, new SubmitOrderCallback() {
                            @Override
                            public void onDownloadFinished(String result) {
                                loading.hide();
                                if(result.equals("SUCCESS")){
                                    Toast.makeText(getApplicationContext(),"تم تنفيذ الطلبية",Toast.LENGTH_SHORT).show();
                                    setResult(RESULT_OK,new Intent().putExtra("submitted","yes"));
                                    finish();
                                }else {
                                    Toast.makeText(getApplicationContext(),"لم يتم التنفيذ",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        submitOrder.execute();
                    }
                });
                builder.setNegativeButton("الغاء",null);
                builder.show();
            }else {
                Toast.makeText(getApplicationContext(),"لا يوجد منتجات في الطلبية",Toast.LENGTH_SHORT).show();
            }
        }else if (id==R.id.order_edit_menu_remove_order){
            AlertDialog.Builder builder = new AlertDialog.Builder(OrderEditActivity.this);
            builder.setTitle("حذف الطلبية ؟");
            builder.setPositiveButton("حذف", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    final Loading loading = new Loading(OrderEditActivity.this,"جاري حذف الطلبية");
                    loading.show();
                    JSONObject orderData = new JSONObject();
                    try {
                        orderData.put("orderID",orderID);
                        orderData.put("comment","");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    RemoveOrder submitOrder = new RemoveOrder(new MyID(getApplicationContext()).getID(), orderData.toString(), new RemoveOrderCallback() {
                        @Override
                        public void onDownloadFinished(String result) {
                            loading.hide();
                            if(result.equals("SUCCESS")){
                                Toast.makeText(getApplicationContext(),"تم حذف الطلبية",Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK,new Intent().putExtra("submitted","yes"));
                                finish();
                            }else {
                                Toast.makeText(getApplicationContext(),"لم يتم التنفيذ",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    submitOrder.execute();
                }
            });
            builder.setNegativeButton("الغاء",null);
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            if(requestCode==777){
                try {
                    String added=data.getStringExtra("added");
                    if(added.equals("yes")){
                        Intent intent = new Intent(getApplicationContext(),OrderEditActivity.class);
                        intent.putExtra("orderID",orderID);
                        startActivity(intent);
                        finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
