package com.batteryhome.orderslibrary.Orders_Package;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.batteryhome.orderslibrary.Callback_Package.OrdersHistoryCallback;
import com.batteryhome.orderslibrary.Customer_Orders_Package.OrderDetailsActivity;
import com.batteryhome.orderslibrary.Loading;
import com.batteryhome.orderslibrary.MyID;
import com.batteryhome.orderslibrary.R;
import com.batteryhome.orderslibrary.ServerData_Package.GetDelegateOrdersHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MyOrdersHistoryFragment extends Fragment {
    private ListView ordersListView;
    private OrdersAdapter ordersAdapter;
    private List<OrderInfo> orderInfoList = new ArrayList<>();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MyOrdersHistoryFragment() {
        // Required empty public constructor
    }

    public static MyOrdersHistoryFragment newInstance(String param1, String param2) {
        MyOrdersHistoryFragment fragment = new MyOrdersHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_orders_history, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void initOpenedOrders(){
        ordersListView = (ListView) getActivity().findViewById(R.id.orders_history_listView);
        ordersAdapter = new OrdersAdapter(getActivity(),orderInfoList);
        ordersListView.setAdapter(ordersAdapter);
        ordersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                OrderInfo info = (OrderInfo) adapterView.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                intent.putExtra("orderID",info.getOrderID()+"");
                startActivity(intent);
            }
        });
        final Loading loading =new Loading(getActivity(),"جاري تحميل الطلبيات");
        loading.show();
        GetDelegateOrdersHistory openedOrders = new GetDelegateOrdersHistory(new MyID(getActivity()).getID(), new OrdersHistoryCallback() {
            @Override
            public void onDownloadFinished(String result) {
                loading.hide();
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo= jsonArray.getJSONObject(i);
                            orderInfoList.add(new OrderInfo(
                                    jo.getInt("orderID"),
                                    jo.getInt("Customer_AddressID"),
                                    jo.getInt("CustomerID"),
                                    jo.getString("customerName"),
                                    jo.getString("assignDate")
                            ));
                            ordersAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getActivity(),"لا يوجد سجل فواتير",Toast.LENGTH_SHORT).show();
                }
            }
        });
        openedOrders.execute();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
