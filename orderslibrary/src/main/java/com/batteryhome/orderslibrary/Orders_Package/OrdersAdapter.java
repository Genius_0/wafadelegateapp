package com.batteryhome.orderslibrary.Orders_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.batteryhome.orderslibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 05-Apr-18.
 */

public class OrdersAdapter extends BaseAdapter {
    private List<OrderInfo> orderInfoList = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public OrdersAdapter(Context context, List<OrderInfo> orderInfoList ) {
        this.orderInfoList = orderInfoList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return orderInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.orders_template,null);
            holder.Index = (TextView) view.findViewById(R.id._order_index);
            holder.OrderID = (TextView) view.findViewById(R.id._order_orderID_);
            holder.CustomerName = (TextView) view.findViewById(R.id._order_customer_name);
            holder.Date = (TextView) view.findViewById(R.id._order_date_);
        }else {
            view = (View) view.getTag();
        }
        holder.Index.setText((i+1)+"");
        holder.OrderID.setText(orderInfoList.get(i).getOrderID()+"");
        holder.CustomerName.setText(orderInfoList.get(i).getCustomerName());
        holder.Date.setText(orderInfoList.get(i).getDate());
        return view;
    }
    private class ViewHolder {
        TextView Index;
        TextView OrderID;
        TextView CustomerName;
        TextView Date;
    }
}