package com.batteryhome.orderslibrary.Callback_Package;

/**
 * Created by ali on 26-Dec-17.
 */

public interface OrdersHistoryCallback {
    void onDownloadFinished(String result);
}
