package com.batteryhome.orderslibrary.Callback_Package;

/**
 * Created by ali on 26-Mar-18.
 */

public interface OrderProcessCallBack {
    void onDownloadFinished(String result);
}
