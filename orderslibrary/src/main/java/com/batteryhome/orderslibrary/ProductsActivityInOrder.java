package com.batteryhome.orderslibrary;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.batteryhome.orderslibrary.Callback_Package.AddProductToOrderCallback;
import com.batteryhome.orderslibrary.ServerData_Package.AddProductToOrder;
import com.batteryhome.productslibrary.Classes_Callback_Package.SelectedProductCallback;
import com.batteryhome.productslibrary.Products_Package.ProductQuantityInDialog;
import com.batteryhome.productslibrary.Products_Package.ProductsFragment;
import com.batteryhome.productslibrary.Products_Package.ProductsInfo;

import org.json.JSONObject;

public class ProductsActivityInOrder extends AppCompatActivity implements ProductsFragment.OnFragmentInteractionListener{
    private String orderID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_);
        orderID = getIntent().getStringExtra("orderID");
        android.support.v4.app.FragmentManager fm  = getSupportFragmentManager();
        ProductsFragment fragment = (ProductsFragment) fm.findFragmentById(R.id.fragment_products_inOrders);
        fragment.initProducts(new SelectedProductCallback() {
            @Override
            public void onSelected(final ProductsInfo productInfo) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(ProductsActivityInOrder.this);
                String[] items = new  String[2];
                items[0]="إضافة المنتج";
                items[1]="الكمية المتاحة";
                builder1.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i==0){
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductsActivityInOrder.this);
                            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                            View view = inflater.inflate(R.layout.add_product_in_order_template,null);
                            EditText price_= (EditText) view.findViewById(R.id._product_in_order_price_);
                            final EditText discount_= (EditText) view.findViewById(R.id._product_in_order_discount_);
                            final EditText quantity_= (EditText) view.findViewById(R.id._product_in_order_quantity_);
                            final EditText comment_= (EditText) view.findViewById(R.id._product_in_order_comment);
                            price_.setText("السعر :"+productInfo.getPrice()+" ");
                            builder.setTitle("إضافة منتج للفاتورة ("+productInfo.getProductID()+")");
                            builder.setView(view);
                            builder.setPositiveButton("إضافة",null);
                            builder.setNegativeButton("الغاء",null);
                            final AlertDialog dialog = builder.create();
                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialogInterface) {
                                    Button button=dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                                    button.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            try{
                                                int quantity= Integer.parseInt(quantity_.getText().toString().trim());
                                                String discount= (discount_.getText().toString().trim());
                                                String comment= comment_.getText().toString().trim();
                                                if(quantity>0){
                                                    if(discount.equals(""))
                                                        discount="0";
                                                    JSONObject orderData = new JSONObject();
                                                    orderData.put("storeProductID",productInfo.getStoreProductID()+"");
                                                    orderData.put("price",productInfo.getPrice()+"");
                                                    orderData.put("quantity",quantity+"");
                                                    orderData.put("discount",discount+"");
                                                    orderData.put("comment",comment+"");
                                                    orderData.put("orderID",orderID+"");
                                                    final Loading loading = new Loading(ProductsActivityInOrder.this,"جاري إضافة المنتج في الطلبية");
                                                    loading.show();
                                                    AddProductToOrder addProductToOrder = new AddProductToOrder(new MyID(getApplicationContext()).getID(), orderData.toString(), new AddProductToOrderCallback() {
                                                        @Override
                                                        public void onDownloadFinished(String result) {
                                                            loading.hide();
                                                            switch (result) {
                                                                case "SUCCESS":
                                                                    Toast.makeText(getApplicationContext(), "تم الإضافة", Toast.LENGTH_SHORT).show();
                                                                    setResult(RESULT_OK,new Intent().putExtra("added","yes"));
                                                                    dialog.dismiss();
                                                                    break;
                                                                case "NOT AVAILABLE":
                                                                    Toast.makeText(getApplicationContext(), "الكمية غير متاحة", Toast.LENGTH_SHORT).show();
                                                                    break;
                                                                default:
                                                                    Toast.makeText(getApplicationContext(), "لم تتم الإضافة", Toast.LENGTH_SHORT).show();
                                                                    break;
                                                            }
                                                        }
                                                    });
                                                    addProductToOrder.execute();

                                                }else {
                                                    Toast.makeText(getApplicationContext(),"قم بكتابة الكمية",Toast.LENGTH_SHORT).show();
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                                Toast.makeText(getApplicationContext(),"قم بكتابة الكمية",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            });
                            dialog.show();
                        }else if (i==1){
                            new ProductQuantityInDialog(ProductsActivityInOrder.this,productInfo.getProductID()+"");
                        }
                    }
                });
                builder1.setNegativeButton("الغاء",null);
                builder1.show();

             }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
