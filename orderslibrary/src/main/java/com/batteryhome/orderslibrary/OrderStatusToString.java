package com.batteryhome.orderslibrary;

/**
 * Created by ali on 01-Apr-18.
 */

public class OrderStatusToString {

    public static String get(int i){
        String s="";
        switch (i){
            case 400:
                s="تم حذف الطلبية";
                break;
            case 401:
                s="تم حفظ الطلبية في محفوظات المندوب";
                break;
            case 402:
                s="تم تنفيذ الطلبية من قبل المندوب";
                break;
            case 403:
                s="تم الغاء الطلبية من قبل المندوب";
                break;
            case 404:
                s="طلب الغاء الطلبية من ادارة المبيعات";
                break;
            case 405:
                s="طلب الغاء الطلبية من خدمة العملاء";
                break;
            case 406:
                s="طلب الغاء الطلبية من الحركة";
                break;
            case 407:
                s="طلب الغاء الطلبية من المخزن";
                break;
            case 408:
                s="طلب الغاء الطلبية من السيارة";
                break;
            case 409 :
                s="تم الموافقة علي الغاء الطلبية";
                break;
            case 410 :
                s="تم رفض الغاء الطلبية";
                break;
            case 411 :
                s="تم الموافقة من ادارة المبيعات";
                break;
            case 412 :
                s="تم الرفض من ادارة المبيعات";
                break;
            case 413 :
                s="تم الموافقة علي الغاء الطلبية";
                break;
            case 414 :
                s="تم رفض الغاء الطلبية";
                break;
            case 415 :
                s="تم الموافقة من خدمة العملاء";
                break;
            case 416 :
                s="تم الرفض من خدمة العملاء";
                break;
            case 417 :
                s="تم الموافقة علي الغاء الطلبية";
                break;
            case 418:
                s="تم رفض الغاء الطلبية";
                break;
            case 419 :
                s="تم الموافقة من الحركة";
                break;
            case 420 :
                s="تم الرفض من الحركة";
                break;
            case 421 :
                s="تم الموافقة علي الغاء الطلبية";
                break;
            case 422 :
                s="تم رفض الغاء الطلبية";
                break;
            case 423 :
                s="تم الموافقة من المخزن";
                break;
            case 424 :
                s="تم الرفض من المخزن";
                break;
            case 425 :
                s="تم تحميل الكمية";
                break;
            case 426 :
                s="تم الموافقة علي الغاء الطلبية";
                break;
            case 427 :
                s="تم رفض الغاء الطلبية";
                break;
            case 428 :
                s="تم الاستلام من السيارة";
                break;
            case 429 :
                s="تم الرفض من السيارة";
                break;
            case 430 :
                s="تم التسليم للعميل";
                break;
            case 431 :
                s="لم يتم التسليم للعميل";
                break;
            case 432 :
                s="تم استلام النقدية";
                break;
            case 433 :
                s="الدفع اجل";
                break;
            case 434 :
                s="لم يتم استلام النقدية";
                break;
            case 435 :
                s="تم تسليم النقدية";
                break;
            case 436 :
                s="لم يتم تسليم النقدية";
                break;
            case 437 :
                s="بيع فوري من المعرض او المخزن";
                break;
            case 438 :
                s="بيع فوري من السيارة";
                break;
            case 489 :
                s="تم التسليم الجزئي للعميل";
                break;
        }
        return s;
    }

}
