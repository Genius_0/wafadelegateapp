package com.batteryhome.orderslibrary;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.ProgressBar;

/**
 * Created by ali on 10-Dec-17.
 */

public class Loading {
    private Context context;
    private AlertDialog alertDialog;
    private String title="";
    public Loading(Context context) {
        this.context = context;
    }
    public Loading(Context context, String title) {
        this.context = context;
        this.title=title;
    }
    public void show(){
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setView(new ProgressBar(context));
        if(!title.equals("")){
            builder.setTitle(title);
        }
        builder.setCancelable(false);
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable( android.graphics.Color.TRANSPARENT));
        alertDialog.show();

    }
    public void hide(){
        alertDialog.dismiss();
    }


}
