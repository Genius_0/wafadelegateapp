package com.batteryhome.orderslibrary;

import android.content.Context;
import android.widget.Toast;

import com.batteryhome.orderslibrary.Callback_Package.OpenedOrdersCallback;
import com.batteryhome.orderslibrary.ServerData_Package.OpenNewOrder;

/**
 * Created by ali on 06-Apr-18.
 */

public class OpenNewOrderForCustomer {
    public OpenNewOrderForCustomer(final Context context, String customerID, String customerAddressID) {
        final Loading loading = new Loading(context,"جاري فتح الفاتورة");
        loading.show();
        OpenNewOrder newOrder  = new OpenNewOrder(new MyID(context).getID(), customerID, customerAddressID, new OpenedOrdersCallback() {
            @Override
            public void onDownloadFinished(String result) {
                loading.hide();
                if(result.equals("SUCCESS")){
                    Toast.makeText(context,"تم فتح فاتورة جديدة",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context,"لم تتم العملية",Toast.LENGTH_SHORT).show();
                }
            }
        });
        newOrder.execute();
    }
}
