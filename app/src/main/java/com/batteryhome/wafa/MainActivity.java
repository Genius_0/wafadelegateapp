package com.batteryhome.wafa;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.batteryhome.customerlibrary.CustomersFragment;
import com.batteryhome.wafa.Orders_Package.OrdersActivity;

public class MainActivity extends AppCompatActivity implements CustomersFragment.OnFragmentInteractionListener{
    private TextView customers,orders,products,settings;
    private float textSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customers = (TextView) findViewById(R.id.customers_card);
        orders = (TextView) findViewById(R.id.orders_card);
        products = (TextView) findViewById(R.id.products_card);
        settings = (TextView) findViewById(R.id.settings_card);

        final Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.my_anim);

        textSize=customers.getTextSize();
        customers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(),CustomersActivity.class);
                startActivity(intent);
            }
        });

        customers.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    increaseSize(customers);
                    customers.startAnimation(animation);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    resetSize(customers);
                }
                return false;
            }
        });

        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),OrdersActivity.class);
                startActivity(intent);
            }
        });

        orders.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    increaseSize(orders);
                    orders.startAnimation(animation);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    resetSize(orders);
                }
                return false;
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),ProductsActivity.class);
                startActivity(intent);
            }
        });

        products.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    increaseSize(products);
                    products.startAnimation(animation);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    resetSize(products);
                }
                return false;
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        settings.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    increaseSize(settings);
                    settings.startAnimation(animation);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    resetSize(settings);
                }
                return false;
            }
        });
    }

    private void resetSize(TextView view) {
        view.setTextSize(textSize);
    }

    private void increaseSize(TextView view) {
        view.setTextSize(textSize+10);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
