package com.batteryhome.wafa;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.batteryhome.customerlibrary.Callback_Classes_Package.CustomerIDCallback;
import com.batteryhome.customerlibrary.CustomersFragment;

public class CustomersActivity extends AppCompatActivity implements CustomersFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        setTitle("العملاء");
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        final CustomersFragment fragment = (CustomersFragment) fm.findFragmentById(R.id.fragment);
        fragment.initGui(new CustomerIDCallback() {
            @Override
            public void onSelect(String customerID) {
                fragment.openCustomerPage(customerID);
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
