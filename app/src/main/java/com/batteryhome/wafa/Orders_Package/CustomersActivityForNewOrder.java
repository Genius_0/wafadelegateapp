package com.batteryhome.wafa.Orders_Package;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.batteryhome.customerlibrary.Address_Package.AddressInfo;
import com.batteryhome.customerlibrary.Address_Package.CustomerAddressInDialog;
import com.batteryhome.customerlibrary.Callback_Classes_Package.CustomerAddressesCallback;
import com.batteryhome.customerlibrary.Callback_Classes_Package.CustomerIDCallback;
import com.batteryhome.customerlibrary.CustomersFragment;
import com.batteryhome.orderslibrary.OpenNewOrderForCustomer;
import com.batteryhome.wafa.R;

public class CustomersActivityForNewOrder extends AppCompatActivity implements CustomersFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers_for_order);
        setTitle("العملاء");
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        final CustomersFragment fragment = (CustomersFragment) fm.findFragmentById(R.id.customers_for_order_fragment);
        fragment.initGui(new CustomerIDCallback() {
            @Override
            public void onSelect(final String customerID) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CustomersActivityForNewOrder.this);
                builder.setTitle(" فتح فاتورة لهذا العميل "+customerID+" ؟");
                builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final CustomerAddressInDialog addressInDialog = new CustomerAddressInDialog();
                        addressInDialog.show(CustomersActivityForNewOrder.this, customerID, new CustomerAddressesCallback() {
                            @Override
                            public void onSelect(AddressInfo addressInfo) {
                                addressInDialog.close();
                                new OpenNewOrderForCustomer(CustomersActivityForNewOrder.this,addressInfo.getCustomerID()+"",addressInfo.getAddressID()+"");
                            }
                        });
                    }
                });

                builder.setNegativeButton("الغاء",null);
                builder.show();
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
