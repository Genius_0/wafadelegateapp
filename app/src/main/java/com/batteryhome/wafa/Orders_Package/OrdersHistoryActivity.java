package com.batteryhome.wafa.Orders_Package;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.batteryhome.orderslibrary.Orders_Package.MyOrdersHistoryFragment;
import com.batteryhome.wafa.R;

public class OrdersHistoryActivity extends AppCompatActivity implements MyOrdersHistoryFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);
        setTitle("سجل الطلبيات / الفواتير");
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        MyOrdersHistoryFragment fragment = (MyOrdersHistoryFragment) fm.findFragmentById(R.id.orders_history_fragment);
        fragment.initOpenedOrders();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
