package com.batteryhome.wafa.Orders_Package;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.batteryhome.orderslibrary.Orders_Package.MyOpenedOrdersFragment;
import com.batteryhome.wafa.R;

public class OrdersActivity extends AppCompatActivity implements MyOpenedOrdersFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        setTitle("الطلبيات المحفوظة");
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        MyOpenedOrdersFragment fragment = (MyOpenedOrdersFragment) fm.findFragmentById(R.id.orders_fragment);
        fragment.initOpenedOrders();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.orders_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id._orders_menu_new_order_){
            Intent intent = new Intent(getApplicationContext(),CustomersActivityForNewOrder.class);
            startActivity(intent);
            finish();

        }else if(id==R.id._orders_menu_orders_history_){
            startActivity(new Intent(getApplicationContext(),OrdersHistoryActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
