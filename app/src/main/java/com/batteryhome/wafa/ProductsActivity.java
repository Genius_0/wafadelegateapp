package com.batteryhome.wafa;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.batteryhome.productslibrary.Classes_Callback_Package.SelectedProductCallback;
import com.batteryhome.productslibrary.Products_Package.ProductQuantityInDialog;
import com.batteryhome.productslibrary.Products_Package.ProductsFragment;
import com.batteryhome.productslibrary.Products_Package.ProductsInfo;

public class ProductsActivity extends AppCompatActivity implements ProductsFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        setTitle("المنتجات");
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        ProductsFragment fragment = (ProductsFragment) fm.findFragmentById(R.id.products_fragment);
        fragment.initProducts(new SelectedProductCallback() {
            @Override
            public void onSelected(ProductsInfo productInfo) {
                new ProductQuantityInDialog(ProductsActivity.this,productInfo.getProductID()+"");
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
