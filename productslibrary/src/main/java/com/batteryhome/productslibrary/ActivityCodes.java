package com.batteryhome.productslibrary;

/**
 * Created by ali on 23-Dec-17.
 */

public class ActivityCodes {
    public final static int DELIVER_ORDERS_TO_CUSTOMERS_REQUEST_CODE=369;
    public final static int DELIVER_ORDERS_TO_CUSTOMERS_RESULT_CODE=370;
    public final static int MAPS_ACTIVITY_REQUEST_CODE=169;
    public final static int MAPS_ACTIVITY_RESULT_CODE=170;
    public static final int CAMERA_CAPTURE=747;
}
