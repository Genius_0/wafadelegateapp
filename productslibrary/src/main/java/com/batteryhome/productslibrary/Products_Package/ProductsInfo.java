package com.batteryhome.productslibrary.Products_Package;

/**
 * Created by ali on 25-Mar-18.
 */

public class ProductsInfo {
    private int productID;
    private int storeProductID;
    private int productType;
    private String brandName;
    private String sizeName;
    private String otherDetails;
    private Float price;

    public ProductsInfo(int productID,int storeProductID, int productType, String brandName, String sizeName, String otherDetails,Float price) {
        this.productID = productID;
        this.storeProductID = storeProductID;
        this.productType = productType;
        this.brandName = brandName;
        this.sizeName = sizeName;
        this.otherDetails = otherDetails;
        this.price = price;
    }

    public int getProductID() {
        return productID;
    }

    public int getStoreProductID() {
        return storeProductID;
    }

    public void setStoreProductID(int storeProductID) {
        this.storeProductID = storeProductID;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}

