package com.batteryhome.productslibrary.Products_Package;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.batteryhome.productslibrary.Callback_Package.BatteriesCallback;
import com.batteryhome.productslibrary.Classes_Callback_Package.SelectedProductCallback;
import com.batteryhome.productslibrary.Callback_Package.TiresCallback;
import com.batteryhome.productslibrary.Loading;
import com.batteryhome.productslibrary.R;
import com.batteryhome.productslibrary.ServerData_Package.GetBatteriesData;
import com.batteryhome.productslibrary.ServerData_Package.GetTiresData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ProductsFragment extends Fragment {
    private ListView listView;
    private List<ProductsInfo> productsInfo = new ArrayList<>();
    private ProductsAdapter adapter ;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProductsFragment() {
        // Required empty public constructor
    }


    public static ProductsFragment newInstance(String param1, String param2) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_products, container, false);
    }
    public void initProducts(final SelectedProductCallback callback){
        listView = (ListView) getActivity().findViewById(R.id.products_listview);
        adapter  = new ProductsAdapter(getActivity(),productsInfo);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                final ProductsInfo productsInfo = (ProductsInfo) adapterView.getItemAtPosition(position);
                if (callback!=null)
                    callback.onSelected(productsInfo);
            }
        });
        final Loading loading = new Loading(getActivity());
        loading.show();
        GetTiresData tiresData = new GetTiresData(new TiresCallback() {
            @Override
            public void onDownloadFinished(String result) {
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i=0; i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            productsInfo.add(new ProductsInfo(
                                    jo.getInt("productID"),
                                    jo.getInt("store_productID"),
                                    2,
                                    jo.getString("brandName"),
                                    jo.getString("width")+"/"+jo.getString("ratio")+"R"+jo.getString("diameter"),
                                    jo.getString("countryName")+" - "+jo.getString("year")+"/"+jo.getString("month")+" - "+jo.getString("pattern"),
                                    (float) jo.getDouble("price")
                            ));
                        }
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getActivity(),"لا يوجد بيانات إطارات",Toast.LENGTH_SHORT).show();
                }
                GetBatteriesData batteriesData = new GetBatteriesData(new BatteriesCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        loading.hide();
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i=0; i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    productsInfo.add(new ProductsInfo(
                                            jo.getInt("productID"),
                                            jo.getInt("store_productID"),
                                            1,
                                            jo.getString("brandName"),
                                            jo.getString("sizeName")+" - "+jo.getString("modelName")+"\n"+jo.getInt("ampere")+"A"+" - "+jo.getString("type")+" - "+jo.getString("terminalLayout"),
                                            jo.getString("countryName")+" - "+jo.getString("year")+"/"+jo.getString("month"),
                                            (float) jo.getDouble("price")
                                    ));
                                    adapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getActivity(),"لا يوجد بيانات بطاريات",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                batteriesData.execute();
            }
        });
        tiresData.execute();
        SearchView searchView = (SearchView) getActivity().findViewById(R.id._product_search_);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
