package com.batteryhome.productslibrary.Products_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.batteryhome.productslibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 25-Mar-18.
 */

public class ProductsAdapter extends BaseAdapter implements Filterable{
    private List<ProductsInfo> productInfoList = new ArrayList<>();
    private List<ProductsInfo> productFilterList = new ArrayList<>();
    private ProductFilter productFilter = new ProductFilter();
    private Context context;
    private LayoutInflater inflater;

    public ProductsAdapter(Context context, List<ProductsInfo> productInfoList ) {
        this.productInfoList = productInfoList;
        this.productFilterList = productInfoList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return productInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.products_template,null);
            holder.BrandName = (TextView) view.findViewById(R.id._product_brand);
            holder.SizeName = (TextView) view.findViewById(R.id._product_size);
            holder.OtherDetails = (TextView) view.findViewById(R.id._product_details);
            holder.Num = (TextView) view.findViewById(R.id._num_products);
            holder.Price = (TextView) view.findViewById(R.id._product_price);
        }else {
            view = (View) view.getTag();
        }
        holder.Num.setText((i+1)+"");
        holder.BrandName.setText(productInfoList.get(i).getBrandName() + " ("+productInfoList.get(i).getProductID()+")");
        holder.SizeName.setText(productInfoList.get(i).getSizeName());
        holder.OtherDetails.setText(productInfoList.get(i).getOtherDetails());
        holder.Price.setText(productInfoList.get(i).getPrice()+"");
        return view;
    }
    private class ViewHolder {
        TextView Num;
        TextView BrandName;
        TextView SizeName;
        TextView OtherDetails;
        TextView Price;
    }

    @Override
    public Filter getFilter() {
        if(productFilter==null) {
            productFilter=new ProductFilter();
        }
        return productFilter;
    }

    private class ProductFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<ProductsInfo> filterList = new ArrayList<ProductsInfo>();
                if(constraint.toString().toUpperCase().contains(" ")){
                    for (int i = 0; i < productInfoList.size(); i++) {
                        for (int w=0;w<constraint.toString().toUpperCase().split(" ").length;w++){
                            if ((productInfoList.get(i).getBrandName().toUpperCase())
                                    .contains(constraint.toString().toUpperCase().split(" ")[w]) ||
                                    (productInfoList.get(i).getSizeName().toUpperCase()).contains(constraint.toString().toUpperCase().split(" ")[w]) ||
                                    (productInfoList.get(i).getOtherDetails().toUpperCase()).contains(constraint.toString().toUpperCase().split(" ")[w])
                                    ) {
                                ProductsInfo customersInfo = new ProductsInfo(
                                        productInfoList.get(i).getProductID(),
                                        productInfoList.get(i).getStoreProductID(),
                                        productInfoList.get(i).getProductType(),
                                        productInfoList.get(i).getBrandName(),
                                        productInfoList.get(i).getSizeName(),
                                        productInfoList.get(i).getOtherDetails(),
                                        productInfoList.get(i).getPrice()
                                );
                                if(w==constraint.toString().toUpperCase().split(" ").length-1)
                                    filterList.add(customersInfo);
                            }
                        }
                    }
                }else {
                    for (int i = 0; i < productFilterList.size(); i++) {
                        if ((productFilterList.get(i).getBrandName().toUpperCase())
                                .contains(constraint.toString().toUpperCase()) ||
                                (productFilterList.get(i).getSizeName().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                (productFilterList.get(i).getOtherDetails().toUpperCase()).contains(constraint.toString().toUpperCase())
                                ) {
                            ProductsInfo customersInfo = new ProductsInfo(
                                    productFilterList.get(i).getProductID(),
                                    productFilterList.get(i).getStoreProductID(),
                                    productFilterList.get(i).getProductType(),
                                    productFilterList.get(i).getBrandName(),
                                    productFilterList.get(i).getSizeName(),
                                    productFilterList.get(i).getOtherDetails(),
                                    productFilterList.get(i).getPrice()
                            );
                            filterList.add(customersInfo);
                        }
                    }
                }

                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = productFilterList.size();
                results.values = productFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            productInfoList = (ArrayList<ProductsInfo>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}