package com.batteryhome.productslibrary.Products_Package;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.batteryhome.productslibrary.Callback_Package.FullProductQuantity;
import com.batteryhome.productslibrary.Loading;
import com.batteryhome.productslibrary.ServerData_Package.GetFullProductQuantity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ali on 06-Apr-18.
 */

public class ProductQuantityInDialog {
    public ProductQuantityInDialog(final Context context, final String productID) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String[] items = new String[1];
        items[0]="الكمية المتوفرة";
        final Loading loading = new Loading(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i==0){
                    loading.show();
                    GetFullProductQuantity fullProductQuantity = new GetFullProductQuantity(productID, 0+"", new FullProductQuantity() {
                        @Override
                        public void onDownloadFinished(String result) {
                            loading.hide();
                            AlertDialog.Builder quantityDialog = new AlertDialog.Builder(context);
                            quantityDialog.setTitle("الكمية المتاحة الان");
                            String[] quan = new String[1];
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                quan[0]=jsonObject.getString("quantity");
                                quantityDialog.setItems(quan,null);
                                quantityDialog.show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    fullProductQuantity.execute();
                }
            }
        });
        builder.show();
    }
}
