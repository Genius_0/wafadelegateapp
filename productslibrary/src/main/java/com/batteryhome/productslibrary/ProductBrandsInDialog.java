package com.batteryhome.productslibrary;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.batteryhome.productslibrary.Callback_Package.AllBrandsNameCallback;
import com.batteryhome.productslibrary.Classes_Callback_Package.SelectedBrandNameCallback;
import com.batteryhome.productslibrary.ServerData_Package.GetAllBrandsName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 10-Apr-18.
 */

public class ProductBrandsInDialog {
    public ProductBrandsInDialog(final Context context, final SelectedBrandNameCallback callback) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Brands");
        final Loading loading = new Loading(context,"جاري التحميل");
        loading.show();
        GetAllBrandsName brandsName =new GetAllBrandsName(new AllBrandsNameCallback() {
            @Override
            public void onDownloadFinished(String result) {
                loading.hide();
                if (!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        List<String> brandsList = new ArrayList<>();
                        final List<Integer> brandsListID = new ArrayList<>();
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            brandsList.add(jo.getString("brandName"));
                            brandsListID.add(jo.getInt("product_brandID"));
                        }
                        ListView listView = new ListView(context);
                        BaseAdapter adapter= new ArrayAdapter<String>(context,R.layout.simple_listview_item,R.id.simple_textView,brandsList);
                        listView.setAdapter(adapter);
                        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if(callback!=null)
                                    callback.onSelected(brandsListID.get(i)+"");
                                return false;
                            }
                        });
                        builder.setView(listView);
                        builder.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(context,"لا يوجد بيانات أصناف",Toast.LENGTH_SHORT).show();
                }
            }
        });
        brandsName.execute();
    }
}
