package com.batteryhome.productslibrary;

/**
 * Created by ali on 10-Dec-17.
 */

public class Connections {
    public final static String IP="http://wafagroup.org:49152";
    public final static String PRODUCTS=IP+"/products";
    public final static String CUSTOMERS=IP+"/customers";
    public final static String CUSTOMERS_IMG=IP+"/customersImgs";
    public final static String ORDERS=IP+"/orders";
    public final static String LOGISTIC=IP+"/logistic";
}
