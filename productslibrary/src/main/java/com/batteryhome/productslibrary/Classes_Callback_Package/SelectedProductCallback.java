package com.batteryhome.productslibrary.Classes_Callback_Package;

import com.batteryhome.productslibrary.Products_Package.ProductsInfo;

/**
 * Created by ali on 25-Dec-17.
 */

public interface SelectedProductCallback {
    void onSelected(ProductsInfo productInfo);
}
