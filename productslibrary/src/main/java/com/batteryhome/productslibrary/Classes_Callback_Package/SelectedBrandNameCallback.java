package com.batteryhome.productslibrary.Classes_Callback_Package;

/**
 * Created by ali on 25-Dec-17.
 */

public interface SelectedBrandNameCallback {
    void onSelected(String id);
}
