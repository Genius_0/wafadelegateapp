package com.batteryhome.productslibrary;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ali on 14-Dec-17.
 */

public class MyID {
    private String theID;
    public MyID(Context context) {
        SharedPreferences sharedPreferences =context.getSharedPreferences("memberInfo",Context.MODE_PRIVATE);
        theID=sharedPreferences.getString("memberID","");
    }
    public String getID(){
        return theID;
    }
}
