package com.batteryhome.customerlibrary.Callback_Package;

/**
 * Created by ali on 26-Dec-17.
 */

public interface DelegateCustomersCallback {
    void onDownloadFinished(String result);
}
