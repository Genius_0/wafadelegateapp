package com.batteryhome.customerlibrary.Callback_Package;

/**
 * Created by ali on 26-Mar-18.
 */

public interface CustomerOrdersCallBack {
    void onDownloadFinished(String result);
}
