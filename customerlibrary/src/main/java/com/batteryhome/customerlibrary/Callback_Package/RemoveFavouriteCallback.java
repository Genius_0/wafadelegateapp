package com.batteryhome.customerlibrary.Callback_Package;

/**
 * Created by ali on 25-Dec-17.
 */

public interface RemoveFavouriteCallback {
    void onDownloadFinished(String result);
}
