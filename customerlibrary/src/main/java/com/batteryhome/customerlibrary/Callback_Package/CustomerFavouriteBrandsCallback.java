package com.batteryhome.customerlibrary.Callback_Package;

/**
 * Created by ali on 25-Dec-17.
 */

public interface CustomerFavouriteBrandsCallback {
    void onDownloadFinished(String result);
}
