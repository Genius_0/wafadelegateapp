package com.batteryhome.customerlibrary.Callback_Package;

/**
 * Created by ali on 04-Mar-18.
 */

public interface UpdateCustomerLocationCallback {
    void onDownloadFinished(String result);
}
