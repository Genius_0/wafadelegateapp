package com.batteryhome.customerlibrary.Customer_Orders_Package;

/**
 * Created by ali on 31-Mar-18.
 */

public class CustomerOrderInfo {
    private int orderID;
    private String date;

    public CustomerOrderInfo(int orderID, String date) {
        this.orderID = orderID;
        this.date = date;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
