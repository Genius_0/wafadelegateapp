package com.batteryhome.customerlibrary.Customer_Orders_Package;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Callback_Package.OrderBatteriesCallback;
import com.batteryhome.customerlibrary.Callback_Package.OrderProcessCallBack;
import com.batteryhome.customerlibrary.Callback_Package.OrderTiresCallback;
import com.batteryhome.customerlibrary.Loading;
import com.batteryhome.customerlibrary.OrderStatusToString;
import com.batteryhome.customerlibrary.R;
import com.batteryhome.customerlibrary.ServerData_Package.GetOrderBatteriesByOrderID;
import com.batteryhome.customerlibrary.ServerData_Package.GetOrderProcessesByOrderID;
import com.batteryhome.customerlibrary.ServerData_Package.GetOrderTiresByOrderID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailsActivity extends AppCompatActivity {
    private ListView listView;
    private List<OrderDetailsInfo> orderDetailsList =new ArrayList<>();
    private OrderDetailsAdapter adapter;
    private String orderID;
    private Loading loadingProcess ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        listView = (ListView) findViewById(R.id.order_details_listVew);
        adapter = new OrderDetailsAdapter(getApplicationContext(),orderDetailsList);
        loadingProcess = new Loading(OrderDetailsActivity.this);
        listView.setAdapter(adapter);
        final Loading loading = new Loading(OrderDetailsActivity.this);
        try {
            orderID = getIntent().getStringExtra("orderID");
        }catch (Exception e){
            e.printStackTrace();
        }
        setTitle("رقم الطلبية : "+orderID);
        loading.show();
        GetOrderBatteriesByOrderID orderBatteries = new GetOrderBatteriesByOrderID(orderID, new OrderBatteriesCallback() {
            @Override
            public void onDownloadFinished(String result) {
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            orderDetailsList.add(new OrderDetailsInfo(
                                    jo.getInt("product_orderID"),
                                    jo.getInt("ProductID"),
                                    jo.getString("brandName")+"\n"+jo.getString("sizeName")+"\n"+jo.getString("modelName")+"\n"+jo.getString("type")+" - "+jo.getString("ampere")+"A",
                                    (float) jo.getDouble("price"), (float) jo.getDouble("discount"),jo.getInt("quantity")
                            ));
                            adapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"لا يوجد بطاريات في الطلبية",Toast.LENGTH_SHORT).show();
                }
                GetOrderTiresByOrderID orderTires = new GetOrderTiresByOrderID(orderID, new OrderTiresCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        loading.hide();
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    orderDetailsList.add(new OrderDetailsInfo(
                                            jo.getInt("product_orderID"),
                                            jo.getInt("ProductID"),
                                            jo.getString("brandName")+"\n"+jo.getString("width")+"/"+jo.getString("ratio")+"R"+jo.getString("diameter")+"\n"+jo.getString("countryName"),
                                            (float) jo.getDouble("price"), (float) jo.getDouble("discount"),jo.getInt("quantity")
                                    ));
                                    adapter.notifyDataSetChanged();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لا يوجد إطارات في الطلبية",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                orderTires.execute();
            }
        });
        orderBatteries.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.order_details_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.order_details_menu_order_process){
            loadingProcess.show();
            GetOrderProcessesByOrderID orderProcess = new GetOrderProcessesByOrderID(orderID, new OrderProcessCallBack() {
                @Override
                public void onDownloadFinished(String result) {
                    loadingProcess.hide();
                    if(!result.equals("NO DATA")){
                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            String[] items = new String[jsonArray.length()];
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jo = jsonArray.getJSONObject(i);
                                items[i]= jo.getString("assignDate")+"\n"+OrderStatusToString.get(jo.getInt("status"));
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
                            builder.setTitle("عمليات الطلبية");
                            builder.setItems(items,null);
                            builder.show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        Toast.makeText(getApplicationContext(),"لا يوجد بيانات",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            orderProcess.execute();
        }
        return super.onOptionsItemSelected(item);
    }
}
