package com.batteryhome.customerlibrary.Customer_Orders_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.batteryhome.customerlibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 31-Mar-18.
 */

public class CustomerOrdersAdapter extends BaseAdapter {
    private List<CustomerOrderInfo> orderInfos = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public CustomerOrdersAdapter(Context context, List<CustomerOrderInfo> orderInfos ) {
        this.orderInfos = orderInfos;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderInfos.size();
    }

    @Override
    public Object getItem(int i) {
        return orderInfos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.customer_order_template,null);
            holder.OrderID = (TextView) view.findViewById(R.id._customer_order_OrderID);
            holder.Date = (TextView) view.findViewById(R.id._customer_order_Date);
        }else {
            view = (View) view.getTag();
        }
        holder.OrderID.setText(orderInfos.get(i).getOrderID()+"");
        holder.Date.setText(orderInfos.get(i).getDate());
        return view;
    }
    private class ViewHolder {
        TextView OrderID;
        TextView Date;

    }
}