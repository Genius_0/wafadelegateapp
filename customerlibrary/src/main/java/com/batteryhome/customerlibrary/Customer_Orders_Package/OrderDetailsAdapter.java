package com.batteryhome.customerlibrary.Customer_Orders_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.batteryhome.customerlibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 01-Apr-18.
 */

public class OrderDetailsAdapter extends BaseAdapter {
    private List<OrderDetailsInfo> orderInfos = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public OrderDetailsAdapter(Context context, List<OrderDetailsInfo> orderInfos ) {
        this.orderInfos = orderInfos;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderInfos.size();
    }

    @Override
    public Object getItem(int i) {
        return orderInfos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.order_details_template,null);
            holder.Index = (TextView) view.findViewById(R.id._order_details_index);
            holder.ProductID = (TextView) view.findViewById(R.id._order_details_productID);
            holder.Description = (TextView) view.findViewById(R.id._order_details_productDesc);
            holder.Quantity = (TextView) view.findViewById(R.id._order_details_quantity);
            holder.Price = (TextView) view.findViewById(R.id._order_details_price);
            holder.Total = (TextView) view.findViewById(R.id._order_details_totalPrice);
            holder.Discount = (TextView) view.findViewById(R.id._order_details_discount);
        }else {
            view = (View) view.getTag();
        }
        holder.Index.setText((+1)+"");
        holder.ProductID.setText(orderInfos.get(i).getProductID()+"");
        holder.Description.setText(orderInfos.get(i).getProductDescription()+"");
        holder.Quantity.setText(orderInfos.get(i).getQuantity()+"");
        holder.Price.setText(orderInfos.get(i).getPrice()+"");
        holder.Discount.setText(orderInfos.get(i).getDiscount()+"");
        holder.Total.setText(((orderInfos.get(i).getPrice()*orderInfos.get(i).getQuantity())-orderInfos.get(i).getDiscount())+"");
        return view;
    }
    private class ViewHolder {
        TextView Index;
        TextView ProductID;
        TextView Description;
        TextView Quantity;
        TextView Price;
        TextView Discount;
        TextView Total;

    }
}