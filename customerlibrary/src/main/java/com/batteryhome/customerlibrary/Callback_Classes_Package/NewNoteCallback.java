package com.batteryhome.customerlibrary.Callback_Classes_Package;

/**
 * Created by ali on 26-Dec-17.
 */

public interface NewNoteCallback {
    void onFinish(String result);
}
