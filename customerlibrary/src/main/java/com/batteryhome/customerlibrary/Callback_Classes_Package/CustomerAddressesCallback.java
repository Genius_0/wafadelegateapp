package com.batteryhome.customerlibrary.Callback_Classes_Package;

import com.batteryhome.customerlibrary.Address_Package.AddressInfo;

/**
 * Created by ali on 26-Dec-17.
 */

public interface CustomerAddressesCallback {
    void onSelect(AddressInfo addressInfo);
}
