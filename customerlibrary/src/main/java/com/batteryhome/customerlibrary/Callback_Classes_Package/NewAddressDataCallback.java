package com.batteryhome.customerlibrary.Callback_Classes_Package;

import org.json.JSONObject;

/**
 * Created by ali on 26-Dec-17.
 */

public interface NewAddressDataCallback {
    void onDownloadFinished(JSONObject result);
}
