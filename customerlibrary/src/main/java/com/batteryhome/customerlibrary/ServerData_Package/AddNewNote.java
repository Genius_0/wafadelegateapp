package com.batteryhome.customerlibrary.ServerData_Package;

import android.os.AsyncTask;

import com.batteryhome.customerlibrary.Callback_Package.AddNewNoteCallback;
import com.batteryhome.customerlibrary.Connections;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by ali on 24-Dec-17.
 */

public class AddNewNote extends AsyncTask<String, Void, String> {

    private String result="";
    private String myID;
    private String note;
    private String customerID;
    private JSONObject jsonObject=new JSONObject();
    private final AddNewNoteCallback callback;

    public AddNewNote(String myID, String customerID,String note, AddNewNoteCallback callback) {
        this.myID = myID;
        this.customerID = customerID;
        this.note = note;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Connections.CUSTOMERS+"/addNewCustomerNote");
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("memberID",myID ));
            nameValuePairs.add(new BasicNameValuePair("customerID",customerID));
            nameValuePairs.add(new BasicNameValuePair("note",note));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            result= EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.onDownloadFinished(this.result);
    }

}
