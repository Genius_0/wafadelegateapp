package com.batteryhome.customerlibrary.ServerData_Package;

import android.os.AsyncTask;

import com.batteryhome.customerlibrary.Callback_Package.AddNewCustomerCallback;
import com.batteryhome.customerlibrary.Connections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by ali on 24-Dec-17.
 */

public class AddNewCustomer extends AsyncTask<String, Void, String> {

    private String result="";
    private String myID;
    private String CustomerName;
    private String CityID;
    private String AddressName;
    private String Phone1;
    private String Phone2;
    private String Lat;
    private String Lng;
    private String LineID;
    private String BatteryWork;
    private String TireWork;
    private final AddNewCustomerCallback callback;

    public AddNewCustomer(String myID, String customerName, String cityID, String addressName, String phone1, String phone2, String lat, String lng, String lineID, String batteryWork, String tireWork, AddNewCustomerCallback callback) {
        this.myID = myID;
        CustomerName = customerName;
        CityID = cityID;
        AddressName = addressName;
        Phone1 = phone1;
        Phone2 = phone2;
        Lat = lat;
        Lng = lng;
        LineID = lineID;
        BatteryWork = batteryWork;
        TireWork = tireWork;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Connections.CUSTOMERS+"/addNewCustomer");
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("memberID",myID ));
            nameValuePairs.add(new BasicNameValuePair("customerName",CustomerName ));
            nameValuePairs.add(new BasicNameValuePair("cityID",CityID ));
            nameValuePairs.add(new BasicNameValuePair("addressName",AddressName ));
            nameValuePairs.add(new BasicNameValuePair("phone1",Phone1 ));
            nameValuePairs.add(new BasicNameValuePair("phone2",Phone2 ));
            nameValuePairs.add(new BasicNameValuePair("lat",Lat ));
            nameValuePairs.add(new BasicNameValuePair("lng",Lng ));
            nameValuePairs.add(new BasicNameValuePair("lineID",LineID ));
            nameValuePairs.add(new BasicNameValuePair("batteryWork",BatteryWork ));
            nameValuePairs.add(new BasicNameValuePair("tireWork",TireWork ));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            result= EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.onDownloadFinished(this.result);
    }

}
