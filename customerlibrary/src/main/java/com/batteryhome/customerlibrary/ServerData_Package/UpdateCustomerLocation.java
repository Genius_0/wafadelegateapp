package com.batteryhome.customerlibrary.ServerData_Package;

import android.os.AsyncTask;

import com.batteryhome.customerlibrary.Callback_Package.UpdateCustomerLocationCallback;
import com.batteryhome.customerlibrary.Connections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by ali on 04-Mar-18.
 */

public class UpdateCustomerLocation extends AsyncTask<String, Void, String> {
    private String result="";
    private String MemberID;
    private String CustomerAddressID;
    private String Lat;
    private String Lng;
    private final UpdateCustomerLocationCallback callback;

    public UpdateCustomerLocation(String memberID,String customerAddressID, String lat, String lng, UpdateCustomerLocationCallback callback) {
        CustomerAddressID = customerAddressID;
        Lat = lat;
        Lng = lng;
        MemberID = memberID;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Connections.CUSTOMERS+"/updateCustomerLocation");
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("customerAddressID", CustomerAddressID));
            nameValuePairs.add(new BasicNameValuePair("lat", Lat));
            nameValuePairs.add(new BasicNameValuePair("lng", Lng));
            nameValuePairs.add(new BasicNameValuePair("memberID", MemberID));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            result= EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.onDownloadFinished(this.result);
    }
}