package com.batteryhome.customerlibrary.ServerData_Package;

import android.os.AsyncTask;

import com.batteryhome.customerlibrary.Callback_Package.OrderProcessCallBack;
import com.batteryhome.customerlibrary.Connections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by ali on 26-Mar-18.
 */

public class GetOrderProcessesByOrderID extends AsyncTask<String, Void, String> {

    private String result="";
    private String orderID;
    private final OrderProcessCallBack callback;

    public GetOrderProcessesByOrderID(String orderID, OrderProcessCallBack callback) {
        this.orderID = orderID;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Connections.ORDERS+"/getOrderProcessesByOrderID");
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("orderID",orderID ));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            result= EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.onDownloadFinished(this.result);
    }

}