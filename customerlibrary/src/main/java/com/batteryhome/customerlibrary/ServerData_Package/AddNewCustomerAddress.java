package com.batteryhome.customerlibrary.ServerData_Package;

import android.os.AsyncTask;

import com.batteryhome.customerlibrary.Callback_Package.AddNewCustomerAddressCallback;
import com.batteryhome.customerlibrary.Connections;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by ali on 24-Dec-17.
 */

public class AddNewCustomerAddress extends AsyncTask<String, Void, String> {

    private String result="";
    private String myID;
    private JSONObject jsonObject=new JSONObject();
    private final AddNewCustomerAddressCallback callback;

    public AddNewCustomerAddress(String myID, String customerID, String cityID, String addressName, String phone1, String phone2, String lat, String lng, String lineID, AddNewCustomerAddressCallback callback) {
        this.myID = myID;
        try {
            jsonObject.put("customerID",customerID);
            jsonObject.put("cityID",cityID);
            jsonObject.put("addressName",addressName);
            jsonObject.put("phone1",phone1);
            jsonObject.put("phone2",phone2);
            jsonObject.put("lat",lat);
            jsonObject.put("lng",lng);
            jsonObject.put("lineID",lineID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Connections.CUSTOMERS+"/addNewCustomerAddress");
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("memberID",myID ));
            nameValuePairs.add(new BasicNameValuePair("addressData",jsonObject.toString()));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            result= EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.onDownloadFinished(this.result);
    }

}
