package com.batteryhome.customerlibrary;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Address_Package.AddressInfo;
import com.batteryhome.customerlibrary.Callback_Package.DelegateCustomersWithAddressCallback;
import com.batteryhome.customerlibrary.Callback_Package.UpdateCustomerLocationCallback;
import com.batteryhome.customerlibrary.ServerData_Package.GetDelegateCustomersWithAddress;
import com.batteryhome.customerlibrary.ServerData_Package.UpdateCustomerLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap mMap;
    private String showAllCustomersOnMap="YES";
    private String showOneCustomerOnMap="YES";
    private Loading loading;
    private List<AddressInfo>addressInfoList=new ArrayList<>();
    private boolean clicked=false;
    private String customerAddressID="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        loading = new Loading(MapsActivity.this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        try {
            showAllCustomersOnMap=getIntent().getStringExtra("showAllCustomersOnMap");
            showOneCustomerOnMap=getIntent().getStringExtra("showOneCustomerOnMap");

        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            customerAddressID=getIntent().getStringExtra("customerAddressID");
        }catch (Exception e){
            e.printStackTrace();
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mMap.setMyLocationEnabled(true);
        // Add a marker in Sydney and move the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng( new LatLng(30, 31)));
        if(showAllCustomersOnMap!=null){
            loading.show();
            GetDelegateCustomersWithAddress getCustomers = new GetDelegateCustomersWithAddress(new MyID(getApplicationContext()).getID(), new DelegateCustomersWithAddressCallback() {
                @Override
                public void onDownloadFinished(String result) {
                    if(!result.equals("NO DATA")){
                        try {
                            final JSONArray jsonArray = new JSONArray(result);
                            for (int i =0 ; i<jsonArray.length();i++){
                                JSONObject jo = jsonArray.getJSONObject(i);
                                addressInfoList.add(new AddressInfo(
                                        jo.getInt("customer_addressID"),jo.getInt("CustomerID"),jo.getInt("LineID"),jo.getInt("CityID"),jo.getInt("stateID"),
                                        jo.getString("addressName"),jo.getString("cityName"),jo.getString("stateName"),jo.getString("phone1"),jo.getString("phone2"),jo.getString("lat"),jo.getString("lng"),jo.getString("lineName")
                                ));
                                LatLng loc=new LatLng(Double.parseDouble(jo.getString("lat")),Double.parseDouble(jo.getString("lng")));
                                mMap.addMarker(new MarkerOptions().position(loc).title(jo.getString("customerName")).snippet(jo.getString("phone1")).visible(true));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),"لا يوجد بيانات للعملاء",Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"تأكد من الإتصال بالإنترنت",Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getApplicationContext(),"لا يوجد بيانات للعملاء",Toast.LENGTH_SHORT).show();
                    }
                    loading.hide();
                }
            });
            getCustomers.execute();
        }else if (showOneCustomerOnMap!=null){
            try {
                LatLng loc=new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")),Double.parseDouble(getIntent().getStringExtra("lng")));
                mMap.addMarker(new MarkerOptions().position(loc).title(getIntent().getStringExtra("name")));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc,16.0f));
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"حدث خطأ",Toast.LENGTH_SHORT).show();
            }
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(latLng).title("موقع العميل"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
                }
            });
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                    builder.setTitle("حفظ موقع العميل ؟");
                    builder.setNegativeButton("لا",null);
                    builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final Loading loading = new Loading(MapsActivity.this);
                            loading.show();
                            UpdateCustomerLocation updateCustomerLocation = new UpdateCustomerLocation(new MyID(getApplicationContext()).getID(),customerAddressID, marker.getPosition().latitude+"", marker.getPosition().longitude+"", new UpdateCustomerLocationCallback() {
                                @Override
                                public void onDownloadFinished(String result) {
                                    loading.hide();
                                    if (result.equals("SUCCESS")){
                                        Toast.makeText(getApplicationContext(),"تم تحديث المكان",Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(getApplicationContext(),"لم يتم التحديث",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            updateCustomerLocation.execute();
                        }
                    });
                    builder.show();
                    return false;
                }
            });
        }else {
            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(loc).title("موقع العميل"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                }
            });
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                    builder.setTitle("حفظ موقع العميل ؟");
                    builder.setNegativeButton("لا",null);
                    builder.setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent();
                            intent.putExtra("lat",marker.getPosition().latitude+"");
                            intent.putExtra("lng",marker.getPosition().longitude+"");
                            setResult(ActivityCodes.MAPS_ACTIVITY_RESULT_CODE,intent);
                            MapsActivity.this.finish();
                        }
                    });
                    builder.show();
                    return false;
                }
            });
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
