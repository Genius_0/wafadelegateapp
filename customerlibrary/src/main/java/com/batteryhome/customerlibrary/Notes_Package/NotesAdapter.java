package com.batteryhome.customerlibrary.Notes_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.batteryhome.customerlibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 26-Mar-18.
 */

public class NotesAdapter extends BaseAdapter {
    private List<NoteInfo> noteInfoList = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public NotesAdapter(Context context, List<NoteInfo> noteInfoList ) {
        this.noteInfoList = noteInfoList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return noteInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return noteInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
       ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.note_template,null);
            holder.Date = (TextView) view.findViewById(R.id._note_date);
            holder.Note = (TextView) view.findViewById(R.id._note_note);
            holder.MemberName = (TextView) view.findViewById(R.id._note_memberName);
        }else {
            view = (View) view.getTag();
        }
        holder.Date.setText(noteInfoList.get(i).getDate());
        holder.Note.setText(noteInfoList.get(i).getNote());
        holder.MemberName.setText(noteInfoList.get(i).getMemberName());
        return view;
    }
    private class ViewHolder {
        TextView Date;
        TextView Note;
        TextView MemberName;
    }
}