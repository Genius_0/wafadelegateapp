package com.batteryhome.customerlibrary.Notes_Package;

/**
 * Created by ali on 26-Mar-18.
 */

public class NoteInfo {
    private int noteID;
    private int memberID;
    private String note;
    private String date;
    private String memberName;

    public NoteInfo(int noteID, int memberID, String note, String date, String memberName) {
        this.noteID = noteID;
        this.memberID = memberID;
        this.note = note;
        this.date = date;
        this.memberName = memberName;
    }

    public int getNoteID() {
        return noteID;
    }

    public void setNoteID(int noteID) {
        this.noteID = noteID;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
}
