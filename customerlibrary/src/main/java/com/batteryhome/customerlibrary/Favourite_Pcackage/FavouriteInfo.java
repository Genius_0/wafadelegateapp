package com.batteryhome.customerlibrary.Favourite_Pcackage;

/**
 * Created by ali on 10-Apr-18.
 */

public class FavouriteInfo {
    private int favouriteBrandID;
    private int brandID;
    private String brandName;

    public FavouriteInfo(int favouriteBrandID, int brandID, String brandName) {
        this.favouriteBrandID = favouriteBrandID;
        this.brandID = brandID;
        this.brandName = brandName;
    }

    public int getFavouriteBrandID() {
        return favouriteBrandID;
    }

    public void setFavouriteBrandID(int favouriteBrandID) {
        this.favouriteBrandID = favouriteBrandID;
    }

    public int getBrandID() {
        return brandID;
    }

    public void setBrandID(int brandID) {
        this.brandID = brandID;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
