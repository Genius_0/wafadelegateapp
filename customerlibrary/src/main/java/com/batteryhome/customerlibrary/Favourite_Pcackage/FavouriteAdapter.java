package com.batteryhome.customerlibrary.Favourite_Pcackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.batteryhome.customerlibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 10-Apr-18.
 */

public class FavouriteAdapter extends BaseAdapter {
    private List<FavouriteInfo> favouriteInfoList = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public FavouriteAdapter(Context context, List<FavouriteInfo> favouriteInfoList ) {
        this.favouriteInfoList = favouriteInfoList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return favouriteInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return favouriteInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.simple_listview_item,null);
            holder.BrandName = (TextView) view.findViewById(R.id.simple_textView);
        }else {
            view = (View) view.getTag();
        }
        holder.BrandName.setText(favouriteInfoList.get(i).getBrandName());
        return view;
    }
    private class ViewHolder {
        TextView BrandName;
    }
}