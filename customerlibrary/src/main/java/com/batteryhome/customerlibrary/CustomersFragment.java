package com.batteryhome.customerlibrary;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Callback_Classes_Package.CustomerIDCallback;
import com.batteryhome.customerlibrary.Callback_Package.CustomersCallback;
import com.batteryhome.customerlibrary.Customers_Package.CustomersAdapter;
import com.batteryhome.customerlibrary.Customers_Package.CustomersInfo;
import com.batteryhome.customerlibrary.Customers_Package.NewCustomerActivity;
import com.batteryhome.customerlibrary.ServerData_Package.GetDelegateCustomers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CustomersFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CustomersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CustomersFragment extends Fragment {
    private List<CustomersInfo> customersList = new ArrayList<>();
    private CustomersAdapter adapter ;
    private SearchView searchView;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CustomersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CustomerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CustomersFragment newInstance(String param1, String param2) {
        CustomersFragment fragment = new CustomersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_customers, container, false);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.customers_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id._menu_new_customer){
            Intent intent = new Intent(getActivity(),NewCustomerActivity.class);
            startActivityForResult(intent,351);
        }else if(id==R.id._menu_customers_on_maps){
            Intent intent = new Intent(getActivity(),MapsActivity.class);
            intent.putExtra("showAllCustomersOnMap","yes");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK){
            if(requestCode==351){
                try {
                    if(data.getStringExtra("added").equals("yes")){
                        getActivity().finish();
                    }
                }catch (Exception e){

                }
            }
        }
    }

    public void initGui(final CustomerIDCallback customerIDCallback){
        final ListView listView = (ListView) getActivity().findViewById(R.id._customers_listView);
        adapter = new CustomersAdapter(getActivity(),customersList);
        listView.setAdapter(adapter);
        final Loading loading = new Loading(getActivity(),"جاري تحميل بيانات العملاء");
        loading.show();
        GetDelegateCustomers delegateCustomers = new GetDelegateCustomers(new MyID(getActivity()).getID(), new CustomersCallback() {
            @Override
            public void onDownloadFinished(String result) {
                loading.hide();
                if (!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            customersList.add(new CustomersInfo(jo.getInt("customerID"),jo.getString("customerName"),jo.getString("category")));
                        }
                        adapter.notifyDataSetChanged();
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                CustomersInfo customer= (CustomersInfo) adapterView.getItemAtPosition(i);
                                if(customerIDCallback!=null)
                                    customerIDCallback.onSelect(customer.getCustomerID()+"");
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(),"لا يوجد بيانات عملاء",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(),"لا يوجد بيانات عملاء",Toast.LENGTH_SHORT).show();
                }
            }
        });
        delegateCustomers.execute();
        searchView = (SearchView) getActivity().findViewById(R.id._customers_search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
    }

    public void openCustomerPage(String customerID){
        Intent intent = new Intent(getActivity(),CustomerPageActivity.class);
        intent.putExtra("customerID",customerID);
        startActivity(intent);
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
