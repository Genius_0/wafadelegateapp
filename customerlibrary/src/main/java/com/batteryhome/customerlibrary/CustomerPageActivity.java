package com.batteryhome.customerlibrary;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Address_Package.AddCustomerAddressInDialog;
import com.batteryhome.customerlibrary.Address_Package.AddressAdapter;
import com.batteryhome.customerlibrary.Address_Package.AddressInfo;
import com.batteryhome.customerlibrary.Callback_Classes_Package.NewAddressDataCallback;
import com.batteryhome.customerlibrary.Callback_Classes_Package.NewNoteCallback;
import com.batteryhome.customerlibrary.Callback_Package.AddFavouriteBrandCallback;
import com.batteryhome.customerlibrary.Callback_Package.AddNewCustomerAddressCallback;
import com.batteryhome.customerlibrary.Callback_Package.AddNewNoteCallback;
import com.batteryhome.customerlibrary.Callback_Package.CheckVisitCallback;
import com.batteryhome.customerlibrary.Callback_Package.CustomerAddressByAddressIDCallBack;
import com.batteryhome.customerlibrary.Callback_Package.CustomerAddressCallBack;
import com.batteryhome.customerlibrary.Callback_Package.CustomerFavouriteBrandsCallback;
import com.batteryhome.customerlibrary.Callback_Package.CustomerInfoCallBack;
import com.batteryhome.customerlibrary.Callback_Package.CustomerNotesCallBack;
import com.batteryhome.customerlibrary.Callback_Package.CustomerOrdersCallBack;
import com.batteryhome.customerlibrary.Callback_Package.CustomerWorkFieldCallBack;
import com.batteryhome.customerlibrary.Callback_Package.RateCustomerCallBack;
import com.batteryhome.customerlibrary.Callback_Package.RemoveFavouriteCallback;
import com.batteryhome.customerlibrary.Customer_Orders_Package.CustomerOrderInfo;
import com.batteryhome.customerlibrary.Customer_Orders_Package.CustomerOrdersAdapter;
import com.batteryhome.customerlibrary.Customer_Orders_Package.OrderDetailsActivity;
import com.batteryhome.customerlibrary.Favourite_Pcackage.FavouriteAdapter;
import com.batteryhome.customerlibrary.Favourite_Pcackage.FavouriteInfo;
import com.batteryhome.customerlibrary.Notes_Package.NoteInfo;
import com.batteryhome.customerlibrary.Notes_Package.NotesAdapter;
import com.batteryhome.customerlibrary.ServerData_Package.AddCustomerFavouriteBrand;
import com.batteryhome.customerlibrary.ServerData_Package.AddNewCustomerAddress;
import com.batteryhome.customerlibrary.ServerData_Package.AddNewNote;
import com.batteryhome.customerlibrary.ServerData_Package.CheckCustomerVisit;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerAddress;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerAddressByAddressID;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerFavouriteBrands;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerInfo;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerNotes;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerOrders;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerWorkField;
import com.batteryhome.customerlibrary.ServerData_Package.RemoveCustomerFavouriteBrand;
import com.batteryhome.customerlibrary.ServerData_Package.SetRateCustomer;
import com.batteryhome.productslibrary.Classes_Callback_Package.SelectedBrandNameCallback;
import com.batteryhome.productslibrary.ProductBrandsInDialog;
import com.google.gson.JsonObject;
import com.joooonho.SelectableRoundedImageView;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CustomerPageActivity extends AppCompatActivity {
    private Uri capturedImageUri = null;

    private ListView notesListView;
    private NotesAdapter notesAdapter;
    private List<NoteInfo> noteInfoList =new ArrayList<>();

    private List<AddressInfo> addressInfoList = new ArrayList<>();
    private AddressAdapter addressAdapter;
    private ListView addressListView;

    private ListView customerOrdersListView;

    private JSONObject newAddressData;
    private String customerID="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_page);
        try {
            customerID = getIntent().getStringExtra("customerID");
        }catch (Exception ex){
            ex.printStackTrace();
        }

        InitTabHost();
        InitNotes(customerID);
    }

    public void setCustomerPhoto(final Bitmap bitmap){
        final SelectableRoundedImageView imageView = (SelectableRoundedImageView) findViewById(R.id._customerPhoto);
        imageView.setImageBitmap(bitmap);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CustomerPageActivity.this);
                ImageView imageView1 = new ImageView(CustomerPageActivity.this);
                imageView1.setImageBitmap(bitmap);
                LinearLayout linearLayout = new LinearLayout(CustomerPageActivity.this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                linearLayout.setLayoutParams(params);
                linearLayout.addView(imageView1);
                builder.setView(linearLayout);
                builder.setPositiveButton("التقاط صورة", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            Calendar cal = Calendar.getInstance();
                            File file = new File(Environment.getExternalStorageDirectory(),  (cal.getTimeInMillis()+".jpg"));
                            if(!file.exists()){
                                try {
                                    file.createNewFile();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }else{
                                file.delete();
                                try {
                                    file.createNewFile();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            //use standard intent to capture an image
                            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            capturedImageUri = Uri.fromFile(file);
                            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
                            //we will handle the returned data in onActivityResult
                            startActivityForResult(captureIntent, ActivityCodes.CAMERA_CAPTURE);
                        }catch(ActivityNotFoundException anfe){
                            //display an error message
                            String errorMessage = "الجهاز لا يدعم التصوير";
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.show();
            }
        });
    }

    public void setCustomerName(String text){
        TextView textView = (TextView) findViewById(R.id._customerName);
        textView.setText(text);
    }

    public String getCustomerName(){
        TextView textView = (TextView) findViewById(R.id._customerName);
        return textView.getText().toString();
    }

    public void setFavourites(String text){
        TextView textView = (TextView) findViewById(R.id._favourites);
        textView.setText(text);
    }

    public void setWorkField(String text){
        TextView textView = (TextView) findViewById(R.id._workField);
        textView.setText(text);
    }

    public void setRate(int num){
        RatingBar ratingBar = (RatingBar)findViewById(R.id._ratingBar);
        ratingBar.setRating(num);
    }

    public ListView getNotesListView(){
        return notesListView;
    }

    public NotesAdapter getNotesAdapter(){
        return notesAdapter;
    }

    public void NewCommentBtn(){
        Button button = (Button) findViewById(R.id.addNewComment);
        new NewComment(button, CustomerPageActivity.this, new NewNoteCallback() {
            @Override
            public void onFinish(final String note) {
                final Loading noteLoading = new Loading(CustomerPageActivity.this,"جاري إضافة التعليق");
                noteLoading.show();
                AddNewNote newNote = new AddNewNote(new MyID(getApplicationContext()).getID(), customerID, note, new AddNewNoteCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        noteLoading.hide();
                        if(!result.equals("FAILED")){
                            try {
                                JSONObject object = new JSONObject(result);
                                if(object.getString("status").equals("SUCCESS")){
                                    noteInfoList.add(0,new NoteInfo(Integer.parseInt(object.getString("insertedId")),Integer.parseInt(new MyID(getApplicationContext()).getID()),note,"الان","انا"));
                                    notesAdapter.notifyDataSetChanged();
                                    Toast.makeText(getApplicationContext(),"تم إضافة التعليق",Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getApplicationContext(),"لم يتم إضافة التعليق",Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لم يتم إضافة التعليق",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                newNote.execute();
            }
        });
    }

    //NewComment newComment = new NewComment(button,getActivity());
    public void InitTabHost(){
        NewCommentBtn();
        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();
        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Tab One");
        spec.setContent(R.id.tab1);
        spec.setIndicator("", getResources().getDrawable(R.drawable.ic_home_black_24dp));
        host.addTab(spec);
        //Tab 2
        spec = host.newTabSpec("Tab Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("", getResources().getDrawable(R.drawable.ic_info_black_24dp));
        host.addTab(spec);

        //Tab 3
        spec = host.newTabSpec("Tab Three");
        spec.setContent(R.id.tab3);
        spec.setIndicator("", getResources().getDrawable(R.drawable.ic_comment_black_24dp));
        host.addTab(spec);

        //Tab 4
        spec = host.newTabSpec("Tab Four");
        spec.setContent(R.id.tab4);
        spec.setIndicator("", getResources().getDrawable(R.drawable.ic_filter_center_focus_black_24dp));
        host.addTab(spec);

        //Tab 5
        spec = host.newTabSpec("Tab Five");
        spec.setContent(R.id.tab5);
        spec.setIndicator("", getResources().getDrawable(R.drawable.ic_receipt_black_24dp));
        host.addTab(spec);

        Button addAddress = (Button) findViewById(R.id.add_customer_addressBtn);
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AddCustomerAddressInDialog(CustomerPageActivity.this).initDialog(new NewAddressDataCallback() {
                    @Override
                    public void onDownloadFinished(JSONObject result) {
                        newAddressData=result;
                        startActivityForResult(new Intent(getApplicationContext(),MapsActivity.class), ActivityCodes.MAPS_ACTIVITY_REQUEST_CODE);
                    }
                });
            }
        });

        // init customer address list
        final Loading loading = new Loading(CustomerPageActivity.this);
        loading.show();
        GetCustomerInfo customerInfo = new GetCustomerInfo(customerID, new CustomerInfoCallBack() {
            @Override
            public void onDownloadFinished(String result) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    JSONObject jo = jsonArray.getJSONObject(0);
                    setCustomerName(jo.getString("customerName")+" ("+jo.getString("category")+")");
                    setRate(jo.getInt("rate"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GetCustomerAddress customerAddress = new GetCustomerAddress(customerID, new CustomerAddressCallBack() {
                    @Override
                    public void onDownloadFinished(String result) {

                        try {
                            setCustomerPhoto(((BitmapDrawable) getResources().getDrawable(R.drawable.nophoto)).getBitmap());
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                            URL url = new URL(Connections.CUSTOMERS_IMG+"/"+customerID+".jpg");
                            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            setCustomerPhoto(bmp);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            setCustomerPhoto(((BitmapDrawable) getResources().getDrawable(R.drawable.nophoto)).getBitmap());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //customerImg.execute();
                        loading.hide();
                        if(!result.equals("NO DATA")){

                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i =0; i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    addressInfoList.add(new AddressInfo(
                                            jo.getInt("customer_addressID"),jo.getInt("CustomerID"),jo.getInt("LineID"),jo.getInt("CityID"),jo.getInt("stateID"),
                                            jo.getString("addressName"),jo.getString("cityName"),jo.getString("stateName"),jo.getString("phone1"),jo.getString("phone2"),jo.getString("lat"),jo.getString("lng"),jo.getString("lineName")
                                    ));
                                }
                                addressListView = (ListView) findViewById(R.id._addressListView);
                                addressAdapter = new AddressAdapter(getApplicationContext(),addressInfoList);
                                addressListView.setAdapter(addressAdapter);
                                addressListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                        final AddressInfo info = (AddressInfo) adapterView.getItemAtPosition(position);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerPageActivity.this);
                                        builder.setTitle(getCustomerName());
                                        builder.setPositiveButton("اتصال بالعميل", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                final Intent intent = new Intent(Intent.ACTION_CALL);
                                                if(info.getPhone2().trim().length()>5){
                                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomerPageActivity.this);
                                                    builder1.setTitle(getCustomerName());;
                                                    final String[] phones ={info.getPhone1(),info.getPhone2()};
                                                    builder1.setItems(phones, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int selectedPhone) {
                                                            intent.setData(Uri.parse("tel:"+phones[selectedPhone]));
                                                            startActivity(intent);
                                                        }
                                                    });
                                                    builder1.show();
                                                }else {
                                                    intent.setData(Uri.parse("tel:"+info.getPhone1()));
                                                    startActivity(intent);
                                                }
                                            }
                                        });
                                        builder.setNegativeButton("موقع العميل", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intent = new Intent(CustomerPageActivity.this,MapsActivity.class);
                                                intent.putExtra("lat",info.getLat());
                                                intent.putExtra("lng",info.getLng());
                                                intent.putExtra("customerAddressID",info.getAddressID()+"");
                                                intent.putExtra("showOneCustomerOnMap","YES");
                                                intent.putExtra("name",getCustomerName());
                                                startActivity(intent);
                                                // weight
                                            }
                                        });
                                        builder.show();

                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لا يوجد عنوان",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                customerAddress.execute();
            }
        });
        customerInfo.execute();

        //init customer Info
        GetCustomerWorkField customerWorkField = new GetCustomerWorkField(customerID, new CustomerWorkFieldCallBack() {
            @Override
            public void onDownloadFinished(String result) {
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        String workFields="";
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            workFields+=" * "+jo.getString("fieldName")+" ";
                        }
                        setWorkField(workFields);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    setWorkField("لا يوجد مجالات عمل");
                }
            }
        });
        customerWorkField.execute();
        //init notes
        //init orders
        GetCustomerOrders customerOrders = new GetCustomerOrders(customerID, new CustomerOrdersCallBack() {
            @Override
            public void onDownloadFinished(String result) {
                TextView orderStatus = (TextView) findViewById(R.id.customer_orders_textView);
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        orderStatus.setVisibility(View.GONE);
                        List<CustomerOrderInfo> customerOrderInfoList = new ArrayList<>();
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            customerOrderInfoList.add(new CustomerOrderInfo(jo.getInt("orderID"),jo.getString("assignDate")));
                        }
                        customerOrdersListView= (ListView) findViewById(R.id.customer_orders_listview);
                        CustomerOrdersAdapter ordersAdapter = new CustomerOrdersAdapter(getApplicationContext(),customerOrderInfoList);
                        customerOrdersListView.setAdapter(ordersAdapter);
                        customerOrdersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                CustomerOrderInfo orderInfo = (CustomerOrderInfo) adapterView.getItemAtPosition(i);
                                Intent intent = new Intent(getApplicationContext(), OrderDetailsActivity.class);
                                intent.putExtra("orderID",orderInfo.getOrderID()+"");
                                startActivity(intent);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                        try {
                            orderStatus.setText("لا يوجد طلبيات سابقة");
                        }catch (Exception ex) {
                            Toast.makeText(getApplicationContext(),"لا يوجد طلبيات سابقة",Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {

                    orderStatus.setText("لا يوجد طلبيات سابقة");
                }
            }
        });
        customerOrders.execute();
        final RatingBar ratingBar = (RatingBar)findViewById(R.id._ratingBar);
        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CustomerPageActivity.this);
                builder.setTitle("تقييم العميل");
                final RatingBar bar = new RatingBar(CustomerPageActivity.this);
                bar.setNumStars(5);
                bar.setStepSize(1);
                bar.setProgress(ratingBar.getProgress());
                bar.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                LinearLayout linearLayout = new LinearLayout(CustomerPageActivity.this);
                linearLayout.addView(bar);
                builder.setView(linearLayout);
                builder.setPositiveButton("تقييم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Loading rateLoading = new Loading(CustomerPageActivity.this,"جاري التقييم");
                        rateLoading.show();
                        SetRateCustomer rateCustomer = new SetRateCustomer(new MyID(getApplicationContext()).getID(), customerID, bar.getProgress() + "", new RateCustomerCallBack() {
                            @Override
                            public void onDownloadFinished(String result) {
                                rateLoading.hide();
                                if(result.equals("SUCCESS")){
                                    Toast.makeText(getApplicationContext(),"تم التقييم",Toast.LENGTH_SHORT).show();
                                    setRate(bar.getProgress());
                                }else {
                                    Toast.makeText(getApplicationContext(),"لم يتم التقييم",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        rateCustomer.execute();
                    }
                });
                builder.setNegativeButton("الغاء",null);
                builder.show();
                return false;
            }
        });

        final TextView favourites = (TextView) findViewById(R.id._favourites);
        favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Loading favLoading = new Loading(CustomerPageActivity.this);
                favLoading.show();
                GetCustomerFavouriteBrands favouriteBrands = new GetCustomerFavouriteBrands(customerID, new CustomerFavouriteBrandsCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        favLoading.hide();
                        final AlertDialog.Builder builder = new AlertDialog.Builder(CustomerPageActivity.this);
                        final List<FavouriteInfo> favList = new ArrayList<FavouriteInfo>();
                        final FavouriteAdapter adapter = new FavouriteAdapter(getApplicationContext(),favList);
                        ListView favListView= new ListView(CustomerPageActivity.this);
                        favListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                                final FavouriteInfo info = (FavouriteInfo) adapterView.getItemAtPosition(position);
                                AlertDialog.Builder itemBuilder = new AlertDialog.Builder(CustomerPageActivity.this);
                                itemBuilder.setTitle("حذف هذا الصنف المفضل ؟");
                                itemBuilder.setPositiveButton("حذف", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final Loading removerLoading = new Loading(CustomerPageActivity.this,"جاري الحذف");
                                        removerLoading.show();
                                        RemoveCustomerFavouriteBrand removeFavourite = new RemoveCustomerFavouriteBrand(info.getFavouriteBrandID() + "", new RemoveFavouriteCallback() {
                                            @Override
                                            public void onDownloadFinished(String result) {
                                                removerLoading.hide();
                                                if(result.equals("SUCCESS")){
                                                    Toast.makeText(getApplicationContext(),"تم الحذف",Toast.LENGTH_SHORT).show();
                                                    favList.remove(position);
                                                    adapter.notifyDataSetChanged();
                                                }else {
                                                    Toast.makeText(getApplicationContext(),"لم يتم الحذف",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                        removeFavourite.execute();
                                    }
                                });
                                itemBuilder.setNegativeButton("الغاء",null);
                                itemBuilder.show();
                                return false;
                            }
                        });
                        favListView.setAdapter( adapter);
                        builder.setView(favListView);
                        builder.setTitle("الأصناف المفضلة");
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);

                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    favList.add(new FavouriteInfo(jo.getInt("customer_favourite_brandID"),jo.getInt("BrandID"),jo.getString("brandName")));
                                    adapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لا يوجد أصناف مفضلة",Toast.LENGTH_SHORT).show();
                        }
                        builder.setPositiveButton("إضافة صنف", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                new ProductBrandsInDialog(CustomerPageActivity.this, new SelectedBrandNameCallback() {
                                    @Override
                                    public void onSelected(String brandID) {
                                        final Loading addFavLoading = new Loading(CustomerPageActivity.this,"جاري اضافة الصنف الى المفضلة");
                                        addFavLoading.show();
                                        AddCustomerFavouriteBrand addFavouriteBrand = new AddCustomerFavouriteBrand(new MyID(getApplicationContext()).getID(), customerID, brandID, new AddFavouriteBrandCallback() {
                                            @Override
                                            public void onDownloadFinished(String result) {
                                                addFavLoading.hide();
                                                try {
                                                    JSONObject jsonObject = new JSONObject(result);
                                                    if(jsonObject.getString("status").equals("SUCCESS")){
                                                        Toast.makeText(getApplicationContext(),"تم الإضافة الى المفضلة",Toast.LENGTH_SHORT).show();
                                                    }else {
                                                        Toast.makeText(getApplicationContext(),"لم يتم الإضافة",Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });
                                        addFavouriteBrand.execute();
                                    }
                                });
                            }
                        });
                        builder.setNegativeButton("خروج",null);
                        builder.show();
                    }
                });
                favouriteBrands.execute();
            }
        });
        Button checkVisit = (Button)findViewById(R.id.checkVisitBtn);
        checkVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(addressInfoList.size()==1){
                    final Loading checkLoading= new Loading(CustomerPageActivity.this,"جاري التنفيذ");
                    checkLoading.show();
                    CheckCustomerVisit checkVisit = new CheckCustomerVisit(new MyID(getApplicationContext()).getID(), addressInfoList.get(0).getAddressID()+"", new CheckVisitCallback() {
                        @Override
                        public void onDownloadFinished(String result) {
                            checkLoading.hide();
                            if(result.equals("FAILED")){
                                Toast.makeText(getApplicationContext(),"لم يتم التنفيذ",Toast.LENGTH_SHORT).show();
                            }else {
                                try {
                                    JSONObject jsonObject = new JSONObject(result);
                                    if(jsonObject.getString("action").equals("LEAVE")){
                                        Toast.makeText(getApplicationContext(),"تم المغادرة",Toast.LENGTH_SHORT).show();
                                    }else if(jsonObject.getString("action").equals("ARRIVE")){
                                        Toast.makeText(getApplicationContext(),"تم الوصول",Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(),"لم يتم التنفيذ",Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                    checkVisit.execute();
                }else if (addressInfoList.size()>1){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CustomerPageActivity.this);
                    builder.setTitle("اختر العنوان");
                    String[] adds= new String[addressInfoList.size()];
                    for (int i=0;i<addressInfoList.size();i++){
                        adds[i]=addressInfoList.get(i).getAddressName();
                    }
                    builder.setItems(adds, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final Loading checkLoading= new Loading(CustomerPageActivity.this,"جاري التنفيذ");
                            checkLoading.show();
                            CheckCustomerVisit checkVisit = new CheckCustomerVisit(new MyID(getApplicationContext()).getID(), addressInfoList.get(i).getAddressID()+"", new CheckVisitCallback() {
                                @Override
                                public void onDownloadFinished(String result) {
                                    checkLoading.hide();
                                    if(result.equals("FAILED")){
                                        Toast.makeText(getApplicationContext(),"لم يتم التنفيذ",Toast.LENGTH_SHORT).show();
                                    }else {
                                        try {
                                            JSONObject jsonObject = new JSONObject(result);
                                            if(jsonObject.getString("action").equals("LEAVE")){
                                                Toast.makeText(getApplicationContext(),"تم المغادرة",Toast.LENGTH_SHORT).show();
                                            }else if(jsonObject.getString("action").equals("ARRIVE")){
                                                Toast.makeText(getApplicationContext(),"تم الوصول",Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(),"لم يتم التنفيذ",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                            checkVisit.execute();
                        }
                    });
                    builder.show();
                }

            }
        });
    }

    public ListView InitNotes(String customerID){
        notesListView = (ListView) CustomerPageActivity.this.findViewById(R.id.notesListView);
        GetCustomerNotes customerNotes = new GetCustomerNotes(customerID, new CustomerNotesCallBack() {
            @Override
            public void onDownloadFinished(String result) {
                notesAdapter = new NotesAdapter(getApplicationContext(),noteInfoList);
                notesListView.setAdapter(notesAdapter);
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i=0; i<jsonArray.length();i++){
                        JSONObject jo = jsonArray.getJSONObject(i);
                        noteInfoList.add(new NoteInfo(
                                jo.getInt("customer_noteID"),jo.getInt("MemberID"),jo.getString("note"),jo.getString("assignDate"),jo.getString("memberName")
                        ));
                        notesAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        customerNotes.execute();
        return notesListView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == ActivityCodes.CAMERA_CAPTURE){
                //get the Uri for the captured image
                try {
                    final Bitmap bitmap = MediaStore.Images.Media.getBitmap( getApplicationContext().getContentResolver(),  capturedImageUri);
                    final Loading loading = new Loading(CustomerPageActivity.this);
                    loading.show();
                    Ion.with(getApplicationContext())
                            .load(Connections.CUSTOMERS+"/uploadImg")
                            .setMultipartParameter("name", customerID+"")
                            .setMultipartFile("image", "image/jpeg",
                                    new File(getRealPathFromURI(getImageUri(getApplicationContext(),bitmap))))
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    loading.hide();
                                    setCustomerPhoto(bitmap);
                                }
                            });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        if(requestCode==ActivityCodes.MAPS_ACTIVITY_REQUEST_CODE && resultCode==ActivityCodes.MAPS_ACTIVITY_RESULT_CODE ){
            try {
                newAddressData.put("lat",data.getStringExtra("lat"));
                newAddressData.put("lng",data.getStringExtra("lng"));
                final Loading AddLoading = new Loading(CustomerPageActivity.this,"جاري تسجيل العنوان");
                AddLoading.show();
                AddNewCustomerAddress newCustomerAddress =
                        new AddNewCustomerAddress(
                                new MyID(getApplicationContext()).getID(),
                                customerID,
                                newAddressData.getString("cityID"),
                                newAddressData.getString("addressName"),
                                newAddressData.getString("phone1"),
                                newAddressData.getString("phone2"),
                                newAddressData.getString("lat"),
                                newAddressData.getString("lng"),
                                newAddressData.getString("lineID"),
                                new AddNewCustomerAddressCallback() {
                                    @Override
                                    public void onDownloadFinished(String result) {
                                        AddLoading.hide();
                                        if(!result.equals("FAILED")){
                                            try {
                                                JSONObject object = new JSONObject(result);
                                                if(object.getString("status").equals("SUCCESS")){
                                                    Toast.makeText(getApplicationContext(),"تم إضافة العنوان",Toast.LENGTH_SHORT).show();
                                                    final Loading addressLoading = new Loading(CustomerPageActivity.this,"جاري تحميل العنوان");
                                                    addressLoading.show();
                                                    GetCustomerAddressByAddressID customerAddressByAddressID = new GetCustomerAddressByAddressID(object.getString("insertedId"), new CustomerAddressByAddressIDCallBack() {
                                                        @Override
                                                        public void onDownloadFinished(String result) {
                                                            addressLoading.hide();
                                                            if(!result.equals("NO DATA")){
                                                                try {
                                                                    JSONArray jsonArray = new JSONArray(result);
                                                                    for (int i =0; i<jsonArray.length();i++){
                                                                        JSONObject jo = jsonArray.getJSONObject(i);
                                                                        addressInfoList.add(0,new AddressInfo(
                                                                                jo.getInt("customer_addressID"),jo.getInt("CustomerID"),jo.getInt("LineID"),jo.getInt("CityID"),jo.getInt("stateID"),
                                                                                jo.getString("addressName"),jo.getString("cityName"),jo.getString("stateName"),jo.getString("phone1"),jo.getString("phone2"),jo.getString("lat"),jo.getString("lng"),jo.getString("lineName")
                                                                        ));
                                                                    }
                                                                    addressAdapter.notifyDataSetChanged();
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                    addressLoading.hide();
                                                                    Toast.makeText(getApplicationContext(),"لا يوجد عنوان",Toast.LENGTH_SHORT).show();
                                                                }
                                                            }else {
                                                                Toast.makeText(getApplicationContext(),"لا يوجد عنوان",Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                                    customerAddressByAddressID.execute();
                                                }else {
                                                    Toast.makeText(getApplicationContext(),"لم يتم إضافة العنوان",Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(),"لم يتم إضافة العنوان",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                        );
                newCustomerAddress.execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
