package com.batteryhome.customerlibrary.Customers_Package;

/**
 * Created by ali on 02-Apr-18.
 */

public class CustomersInfo  {
    private int customerID;
    private String customerName;
    private String category;

    public CustomersInfo(int customerID, String customerName, String category) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.category = category;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
