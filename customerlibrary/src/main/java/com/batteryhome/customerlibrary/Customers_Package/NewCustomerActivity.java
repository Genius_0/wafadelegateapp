package com.batteryhome.customerlibrary.Customers_Package;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.batteryhome.customerlibrary.ActivityCodes;
import com.batteryhome.customerlibrary.Callback_Package.AddNewCustomerCallback;
import com.batteryhome.customerlibrary.Callback_Package.AllLinesCallback;
import com.batteryhome.customerlibrary.Callback_Package.CityCallback;
import com.batteryhome.customerlibrary.Callback_Package.StatesCallback;
import com.batteryhome.customerlibrary.Loading;
import com.batteryhome.customerlibrary.MapsActivity;
import com.batteryhome.customerlibrary.MyID;
import com.batteryhome.customerlibrary.R;
import com.batteryhome.customerlibrary.ServerData_Package.AddNewCustomer;
import com.batteryhome.customerlibrary.ServerData_Package.GetAllLines;
import com.batteryhome.customerlibrary.ServerData_Package.GetAllStates;
import com.batteryhome.customerlibrary.ServerData_Package.GetCityByStateID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NewCustomerActivity extends AppCompatActivity {
    private EditText name,phone1,phone2,address;
    private TextView state,city,line;
    private Button save;
    private CheckBox tireCheckBox,batteryCheckBox;
    private Loading loading;
    private String myID;
    private String stateID="";
    private String cityID="";
    private String lineID="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        name = (EditText) findViewById(R.id.new_customer_customerName);
        address = (EditText) findViewById(R.id.new_customer_addressName);
        state = (TextView) findViewById(R.id.new_customer_stateName);
        city = (TextView) findViewById(R.id.new_customer_cityName);
        line= (TextView) findViewById(R.id.new_customer_lineName);
        phone1 = (EditText) findViewById(R.id.new_customer_phone1);
        phone2 = (EditText) findViewById(R.id.new_customer_phone2);
        save = (Button) findViewById(R.id.new_customer_saveButton);
        tireCheckBox= (CheckBox) findViewById(R.id.new_customer_checkBoxTire);
        batteryCheckBox= (CheckBox) findViewById(R.id.new_customer_checkBoxBattery);
        myID=new MyID(getApplicationContext()).getID();
        loading= new Loading(NewCustomerActivity.this);

        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading.show();
                stateID = "";
                state.setText("");
                city.setText("");
                cityID="";
                GetAllStates getAllStates = new GetAllStates(myID, new StatesCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                final String[] statesList= new String[jsonArray.length()];
                                final Integer[] statesIDsList= new Integer[jsonArray.length()];
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    statesList[i]=jo.getString("stateName");
                                    statesIDsList[i]=jo.getInt("stateID");
                                }
                                AlertDialog.Builder statesBuilder = new AlertDialog.Builder(NewCustomerActivity.this);
                                statesBuilder.setItems(statesList, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        stateID=statesIDsList[i]+"";
                                        state.setText(statesList[i]);
                                    }
                                });
                                statesBuilder.setTitle("اختر المحافظة");
                                statesBuilder.show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),"لا يوجد بيانات للمحافظات",Toast.LENGTH_SHORT).show();
                                Toast.makeText(getApplicationContext(),"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لا يوجد بيانات للمحافظات",Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                        }
                        loading.hide();
                    }
                });
                getAllStates.execute();
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                city.setText("");
                cityID="";
                if(!stateID.equals("")){
                    loading.show();
                    GetCityByStateID getCity= new GetCityByStateID(myID, stateID, new CityCallback() {
                        @Override
                        public void onDownloadFinished(String result) {
                            if(!result.equals("NO DATA")){
                                try {
                                    JSONArray jsonArray = new JSONArray(result);
                                    final String[] citiesList= new String[jsonArray.length()];
                                    final Integer[] citiesIDsList= new Integer[jsonArray.length()];
                                    for (int i=0;i<jsonArray.length();i++){
                                        JSONObject jo = jsonArray.getJSONObject(i);
                                        citiesList[i]=jo.getString("cityName");
                                        citiesIDsList[i]=jo.getInt("cityID");
                                    }
                                    AlertDialog.Builder statesBuilder = new AlertDialog.Builder(NewCustomerActivity.this);
                                    statesBuilder.setItems(citiesList, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            cityID=citiesIDsList[i]+"";
                                            city.setText(citiesList[i]);
                                        }
                                    });
                                    statesBuilder.setTitle("اختر االخط");
                                    statesBuilder.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(),"لا يوجد بيانات للمدن",Toast.LENGTH_SHORT).show();
                                    Toast.makeText(getApplicationContext(),"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(getApplicationContext(),"لا يوجد بيانات للمدن",Toast.LENGTH_SHORT).show();
                                Toast.makeText(getApplicationContext(),"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                            }
                            loading.hide();
                        }
                    });
                    getCity.execute();
                }else {
                    Toast.makeText(getApplicationContext(),"اختر المحافظة",Toast.LENGTH_SHORT).show();
                }

            }
        });
        line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                line.setText("");
                lineID="";
                loading.show();
                GetAllLines getCity= new GetAllLines(myID, new AllLinesCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                final String[] linesList= new String[jsonArray.length()];
                                final Integer[] linesIDsList= new Integer[jsonArray.length()];
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    linesList[i]=jo.getString("lineName");
                                    linesIDsList[i]=jo.getInt("lineID");
                                }
                                AlertDialog.Builder statesBuilder = new AlertDialog.Builder(NewCustomerActivity.this);
                                statesBuilder.setItems(linesList, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        lineID=linesIDsList[i]+"";
                                        line.setText(linesList[i]);
                                    }
                                });
                                statesBuilder.setTitle("اختر الخط");
                                statesBuilder.show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),"لا يوجد بيانات للخطوط",Toast.LENGTH_SHORT).show();
                                Toast.makeText(getApplicationContext(),"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"لا يوجد بيانات للخطوط",Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                        }
                        loading.hide();
                    }
                });
                getCity.execute();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cName = name.getText().toString().trim();
                String cAddress = address.getText().toString().trim();
                String cPhone1 = phone1.getText().toString().trim();
                String cPhone2 = phone2.getText().toString().trim();
                if(cName.isEmpty()||cAddress.isEmpty()||cPhone1.isEmpty()||lineID.isEmpty()||cityID.isEmpty()){
                    Toast.makeText(getApplicationContext(),"ادخل بيانات العميل",Toast.LENGTH_SHORT).show();
                }else {
                    startActivityForResult(new Intent(getApplicationContext(),MapsActivity.class), ActivityCodes.MAPS_ACTIVITY_REQUEST_CODE);
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==ActivityCodes.MAPS_ACTIVITY_REQUEST_CODE&&resultCode==ActivityCodes.MAPS_ACTIVITY_RESULT_CODE){
            String cName = name.getText().toString().trim();
            String cAddress = address.getText().toString().trim();
            String cPhone1 = phone1.getText().toString().trim();
            String cPhone2 = phone2.getText().toString().trim();
            String batteryWork="";
            String tireWork="";
            if(batteryCheckBox.isChecked()){
                batteryWork="yes";
            }
            if(tireCheckBox.isChecked()){
                tireWork="yes";
            }
            if(data.getStringExtra("lat").isEmpty()||data.getStringExtra("lat").equals("null")||
                    data.getStringExtra("lng").isEmpty()||data.getStringExtra("lng").equals("null")){
                Toast.makeText(getApplicationContext(),"اختر مكان العميل على الخريطة",Toast.LENGTH_SHORT).show();
            }else {
                loading.show();
                // SEND NEW CUSTOMER DATA TO SERVER
                AddNewCustomer newCustomer = new AddNewCustomer(myID, cName, cityID, cAddress, cPhone1, cPhone2, data.getStringExtra("lat"), data.getStringExtra("lng"), lineID, batteryWork, tireWork, new AddNewCustomerCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        loading.hide();
                        if(result.equals("SUCCESS")){
                            Toast.makeText(getApplicationContext(),"تم إضافة العميل",Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK,new Intent().putExtra("added","yes"));
                            NewCustomerActivity.this.finish();
                        }else {
                            Toast.makeText(getApplicationContext(),"لم تتم الإضافة",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                newCustomer.execute();
            }
        }
    }
}
