package com.batteryhome.customerlibrary.Customers_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.batteryhome.customerlibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 02-Apr-18.
 */

public class CustomersAdapter extends BaseAdapter implements Filterable {
    private List<CustomersInfo> customersInfoList = new ArrayList<>();
    private List<CustomersInfo> customersListFiltered = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;
    private CustomerFilter customerFilter;

    public CustomersAdapter(Context context, List<CustomersInfo> customersInfoList ) {
        this.customersInfoList = customersInfoList;
        this.customersListFiltered = customersInfoList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return customersInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return customersInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.customers_template,null);
            holder.Index = (TextView) view.findViewById(R.id._customers_index);
            holder.CustomerID = (TextView) view.findViewById(R.id._customers_customerID);
            holder.CustomerName = (TextView) view.findViewById(R.id._customers_customerName);
            holder.Category = (TextView) view.findViewById(R.id._customers_category);
        }else {
            view = (View) view.getTag();
        }
        holder.Index.setText((i+1)+"");
        holder.CustomerID.setText(customersInfoList.get(i).getCustomerID()+"");
        holder.CustomerName.setText(customersInfoList.get(i).getCustomerName()+"");
        holder.Category.setText(customersInfoList.get(i).getCategory()+"");
        return view;
    }
    private class ViewHolder {
        TextView Index;
        TextView CustomerID;
        TextView CustomerName;
        TextView Category;
    }

    @Override
    public Filter getFilter() {
        if(customerFilter==null) {

            customerFilter=new CustomerFilter();
        }

        return customerFilter;
    }

    private class CustomerFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<CustomersInfo> filterList = new ArrayList<CustomersInfo>();
                for (int i = 0; i < customersListFiltered.size(); i++) {
                    if ((customersListFiltered.get(i).getCustomerName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())||
                            (customersListFiltered.get(i).getCustomerID()+"")
                                    .contains(constraint.toString().toUpperCase())) {
                        CustomersInfo customersInfo = new CustomersInfo(
                                customersListFiltered.get(i).getCustomerID(),
                                customersListFiltered.get(i).getCustomerName(),
                                customersListFiltered.get(i).getCategory());
                        filterList.add(customersInfo);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = customersListFiltered.size();
                results.values = customersListFiltered;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            customersInfoList = (ArrayList<CustomersInfo>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}