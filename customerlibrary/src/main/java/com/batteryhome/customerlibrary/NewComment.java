package com.batteryhome.customerlibrary;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Callback_Classes_Package.NewNoteCallback;

/**
 * Created by ali on 26-Mar-18.
 */

public class NewComment  {

    public NewComment(Button button, final Context context, final NewNoteCallback noteCallback) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.new_comment_dialog,null);
                final EditText commentText = (EditText) linearLayout.findViewById(R.id._addNewCommentTextDialog);
                Button add = (Button) linearLayout.findViewById(R.id._addNewCommentBtnDialog);
                builder.setTitle("تعليق");
                builder.setView(linearLayout);
                final AlertDialog dialog= builder.create();
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String comment = commentText.getText().toString().trim();
                        if(!comment.isEmpty()){
                            Toast.makeText(context,comment,Toast.LENGTH_SHORT).show();
                            noteCallback.onFinish(comment);
                            dialog.dismiss();
                        }
                    }
                });
                dialog.show();

            }
        });
    }

}
