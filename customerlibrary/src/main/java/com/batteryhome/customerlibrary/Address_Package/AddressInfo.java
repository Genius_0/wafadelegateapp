package com.batteryhome.customerlibrary.Address_Package;

/**
 * Created by ali on 26-Mar-18.
 */

public class AddressInfo {
    private int addressID;
    private int customerID;
    private int lineID;
    private int cityID;
    private int stateID;
    private String addressName;
    private String phone1;
    private String phone2;
    private String lat;
    private String lng;
    private String lineName;
    private String cityName;
    private String stateName;

    public AddressInfo(int addressID, int customerID, int lineID, int cityID, int stateID, String addressName,String cityName,String stateName, String phone1, String phone2, String lat, String lng, String lineName) {
        this.addressID = addressID;
        this.customerID = customerID;
        this.lineID = lineID;
        this.cityID = cityID;
        this.stateID = stateID;
        this.addressName = addressName;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.lat = lat;
        this.lng = lng;
        this.lineName = lineName;
        this.cityName = cityName;
        this.stateName = stateName;
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getLineID() {
        return lineID;
    }

    public void setLineID(int lineID) {
        this.lineID = lineID;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public int getStateID() {
        return stateID;
    }

    public void setStateID(int stateID) {
        this.stateID = stateID;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
