package com.batteryhome.customerlibrary.Address_Package;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Callback_Classes_Package.NewAddressDataCallback;
import com.batteryhome.customerlibrary.Callback_Package.AllLinesCallback;
import com.batteryhome.customerlibrary.Callback_Package.CityCallback;
import com.batteryhome.customerlibrary.Callback_Package.StatesCallback;
import com.batteryhome.customerlibrary.Loading;
import com.batteryhome.customerlibrary.MyID;
import com.batteryhome.customerlibrary.R;
import com.batteryhome.customerlibrary.ServerData_Package.GetAllLines;
import com.batteryhome.customerlibrary.ServerData_Package.GetAllStates;
import com.batteryhome.customerlibrary.ServerData_Package.GetCityByStateID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by ali on 30-Mar-18.
 */

public class AddCustomerAddressInDialog  {
    private Context context;
    private String stateID="";
    private String cityID="";
    private String lineID="";
    private String customerID;
    private String myID;
    public AddCustomerAddressInDialog(Context context) {
        this.context = context;
    }
    public void initDialog(final NewAddressDataCallback addressDataCallback){
        myID= new MyID(context).getID();
        final Loading loading = new Loading(context);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.new_customer_address_dialog,null);
        builder.setTitle("إضافة عنوان");
        builder.setView(view);
        final EditText address = (EditText) view.findViewById(R.id._new_address_dialog_addressName);
        final EditText phone1 = (EditText) view.findViewById(R.id._new_address_dialog_phone1);
        final EditText phone2 = (EditText) view.findViewById(R.id._new_address_dialog_phone2);
        final TextView state= (TextView) view.findViewById(R.id._new_address_dialog_state);
        final TextView line= (TextView) view.findViewById(R.id._new_address_dialog_line);
        final TextView city= (TextView) view.findViewById(R.id._new_address_dialog_city);
        final Button save= (Button) view.findViewById(R.id._new_address_dialog_saveBtn);

        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading.show();
                stateID = "";
                state.setText("اختر المحافظة");
                city.setText("اختر المدينة");
                cityID="";
                GetAllStates getAllStates = new GetAllStates(myID, new StatesCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                final String[] statesList= new String[jsonArray.length()];
                                final Integer[] statesIDsList= new Integer[jsonArray.length()];
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    statesList[i]=jo.getString("stateName");
                                    statesIDsList[i]=jo.getInt("stateID");
                                }
                                AlertDialog.Builder statesBuilder = new AlertDialog.Builder(context);
                                statesBuilder.setItems(statesList, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        stateID=statesIDsList[i]+"";
                                        state.setText(statesList[i]);
                                    }
                                });
                                statesBuilder.setTitle("اختر المحافظة");
                                statesBuilder.show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context,"لا يوجد بيانات للمحافظات",Toast.LENGTH_SHORT).show();
                                Toast.makeText(context,"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context,"لا يوجد بيانات للمحافظات",Toast.LENGTH_SHORT).show();
                            Toast.makeText(context,"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                        }
                        loading.hide();
                    }
                });
                getAllStates.execute();
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                city.setText("اختر المدينة");
                cityID="";
                if(!stateID.equals("")){
                    loading.show();
                    GetCityByStateID getCity= new GetCityByStateID(myID, stateID, new CityCallback() {
                        @Override
                        public void onDownloadFinished(String result) {
                            if(!result.equals("NO DATA")){
                                try {
                                    JSONArray jsonArray = new JSONArray(result);
                                    final String[] citiesList= new String[jsonArray.length()];
                                    final Integer[] citiesIDsList= new Integer[jsonArray.length()];
                                    for (int i=0;i<jsonArray.length();i++){
                                        JSONObject jo = jsonArray.getJSONObject(i);
                                        citiesList[i]=jo.getString("cityName");
                                        citiesIDsList[i]=jo.getInt("cityID");
                                    }
                                    AlertDialog.Builder statesBuilder = new AlertDialog.Builder(context);
                                    statesBuilder.setItems(citiesList, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            cityID=citiesIDsList[i]+"";
                                            city.setText(citiesList[i]);
                                        }
                                    });
                                    statesBuilder.setTitle("اختر المدينة");
                                    statesBuilder.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(context,"لا يوجد بيانات للمدن",Toast.LENGTH_SHORT).show();
                                    Toast.makeText(context,"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(context,"لا يوجد بيانات للمدن",Toast.LENGTH_SHORT).show();
                                Toast.makeText(context,"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                            }
                            loading.hide();
                        }
                    });
                    getCity.execute();
                }else {
                    Toast.makeText(context,"اختر المحافظة",Toast.LENGTH_SHORT).show();
                }

            }
        });
        line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                line.setText("اختر الخط");
                lineID="";
                loading.show();
                GetAllLines getCity= new GetAllLines(myID, new AllLinesCallback() {
                    @Override
                    public void onDownloadFinished(String result) {
                        if(!result.equals("NO DATA")){
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                final String[] linesList= new String[jsonArray.length()];
                                final Integer[] linesIDsList= new Integer[jsonArray.length()];
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    linesList[i]=jo.getString("lineName");
                                    linesIDsList[i]=jo.getInt("lineID");
                                }
                                AlertDialog.Builder statesBuilder = new AlertDialog.Builder(context);
                                statesBuilder.setItems(linesList, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        lineID=linesIDsList[i]+"";
                                        line.setText(linesList[i]);
                                    }
                                });
                                statesBuilder.setTitle("اختر الخط");
                                statesBuilder.show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context,"لا يوجد بيانات للخطوط",Toast.LENGTH_SHORT).show();
                                Toast.makeText(context,"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context,"لا يوجد بيانات للخطوط",Toast.LENGTH_SHORT).show();
                            Toast.makeText(context,"لا يمكنك تسجيل عميل جديد",Toast.LENGTH_SHORT).show();
                        }
                        loading.hide();
                    }
                });
                getCity.execute();
            }
        });

        builder.setNegativeButton("الغاء",null);
        final AlertDialog dialog = builder.create();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cAddress = address.getText().toString().trim();
                String cPhone1 = phone1.getText().toString().trim();
                String cPhone2 = phone2.getText().toString().trim();
                if(cAddress.isEmpty()||cPhone1.isEmpty()||lineID.isEmpty()||cityID.isEmpty()){
                    Toast.makeText(context,"ادخل بيانات العميل",Toast.LENGTH_SHORT).show();

                }else {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("addressName",cAddress);
                        jsonObject.put("lineID",lineID);
                        jsonObject.put("cityID",cityID);
                        jsonObject.put("phone1",cPhone1);
                        jsonObject.put("phone2",cPhone2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    addressDataCallback.onDownloadFinished(jsonObject);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }
}
