package com.batteryhome.customerlibrary.Address_Package;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.batteryhome.customerlibrary.Callback_Classes_Package.CustomerAddressesCallback;
import com.batteryhome.customerlibrary.Callback_Package.CustomerAddressCallBack;
import com.batteryhome.customerlibrary.R;
import com.batteryhome.customerlibrary.ServerData_Package.GetCustomerAddress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 06-Apr-18.
 */

public class CustomerAddressInDialog {
    private AlertDialog dialog ;

    public void show(final Context context , String customerID, final CustomerAddressesCallback callBack) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("عنوان العميل");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.customer_address_layout_temp,null);
        builder.setView(view);
        final List<AddressInfo> addressInfoList = new ArrayList<>();
        GetCustomerAddress customerAddress = new GetCustomerAddress(customerID, new CustomerAddressCallBack() {
            @Override
            public void onDownloadFinished(String result) {
                if(!result.equals("NO DATA")){
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i =0; i<jsonArray.length();i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            addressInfoList.add(new AddressInfo(
                                    jo.getInt("customer_addressID"),jo.getInt("CustomerID"),jo.getInt("LineID"),jo.getInt("CityID"),jo.getInt("stateID"),
                                    jo.getString("addressName"),jo.getString("cityName"),jo.getString("stateName"),jo.getString("phone1"),jo.getString("phone2"),jo.getString("lat"),jo.getString("lng"),jo.getString("lineName")
                            ));
                        }
                        ListView addressListView = (ListView) view.findViewById(R.id._addressListView);
                        Button button = (Button) view.findViewById(R.id.add_customer_addressBtn);
                        button.setVisibility(View.GONE);
                        AddressAdapter addressAdapter = new AddressAdapter(context,addressInfoList);
                        addressListView.setAdapter(addressAdapter);
                        addressListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                final AddressInfo info = (AddressInfo) adapterView.getItemAtPosition(position);
                                callBack.onSelect(info);

                            }
                        });
                        dialog = builder.create();
                        dialog.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(context,"لا يوجد عنوان",Toast.LENGTH_SHORT).show();
                }
            }
        });
        customerAddress.execute();
    }
    public void close(){
        dialog.dismiss();
    }
}
