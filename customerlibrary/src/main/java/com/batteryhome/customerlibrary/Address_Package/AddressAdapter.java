package com.batteryhome.customerlibrary.Address_Package;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.batteryhome.customerlibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 26-Mar-18.
 */

public class AddressAdapter extends BaseAdapter {
    private List<AddressInfo> addressInfoList = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public AddressAdapter(Context context, List<AddressInfo> addressInfoList ) {
        this.addressInfoList = addressInfoList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return addressInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return addressInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder=null;
        if (holder==null){
            holder=new ViewHolder();
            view = inflater.inflate(R.layout.customer_address_template,null);
            holder.AddressName = (TextView) view.findViewById(R.id._addressName);
            holder.Phone1 = (TextView) view.findViewById(R.id._phone1);
            holder.Phone2 = (TextView) view.findViewById(R.id._phone2);
            holder.LineName = (TextView) view.findViewById(R.id._lineName);
        }else {
            view = (View) view.getTag();
        }
        holder.AddressName.setText(addressInfoList.get(i).getAddressName()+" - "+addressInfoList.get(i).getCityName()+" - "+addressInfoList.get(i).getStateName());
        holder.Phone1.setText(addressInfoList.get(i).getPhone1());
        holder.Phone2.setText(addressInfoList.get(i).getPhone2());
        holder.LineName.setText(addressInfoList.get(i).getLineName());
        return view;
    }
    private class ViewHolder {
        TextView AddressName;
        TextView Phone1;
        TextView Phone2;
        TextView LineName;
    }
}
